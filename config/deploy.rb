set :application, "ats_eis"
set :scm, :git
set :repository,  "."
set :deploy_to, "/var/www/ats"
set :deploy_via, :copy
set :user, "root"

#server "172.16.24.143", :app, :db, :primary => true # Change this to the ip of the server
server "172.16.24.145", :app, :db, :primary => true # Change this to the ip of the server

namespace :deploy do
    task :update do
        transaction do
            update_code
            symlink
            laravel_migrate
        end
    end

    task :symlink do
        transaction do
            run "ln -nfs #{current_release} #{current_path}"
        end
    end

    task :finalize_update do
        transaction do
            run "chown -R www-data:www-data #{release_path}"
            run "chmod -R g+w #{release_path}"
        end
    end

    task :laravel_migrate do
        transaction do
            run "php  #{current_path}/artisan migrate:install"
            run "php  #{current_path}/artisan migrate"
        end
    end

    task :restart do
        transaction do
            # set writable storage dir
            run "chmod -R 777 #{deploy_to}/#{current_dir}/storage/cache"
            run "chmod -R 777 #{deploy_to}/#{current_dir}/storage/database"
            run "chmod -R 777 #{deploy_to}/#{current_dir}/storage/logs"
            run "chmod -R 777 #{deploy_to}/#{current_dir}/storage/sessions"
            run "chmod -R 777 #{deploy_to}/#{current_dir}/storage/views"
            run "chmod -R 777 #{deploy_to}/#{current_dir}/storage/work"
            # remove some junk file
            run "rm -f #{deploy_to}/#{current_dir}/storage/cache/*"
            run "rm -f #{deploy_to}/#{current_dir}/storage/views/*"
        end
    end
end
