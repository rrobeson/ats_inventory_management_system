# ATS Inventory Management System

Designed for Software Engineering by Ryan Robeson, Glenn Addison, Vanessa Hilgadiack, Ashley White, Daniel Pullon, Matthew Bible, and Zachary Fillers

Excellent Documentation for the Laravel Framework is available at [laravel.com](http://laravel.com/docs)

## Custom installing the software (Make your own decisions)
* Setup a server how you like. 
* This application works with Apache(or Nginx), MySQL, and PHP.
* Deploy the code to the directory of your choice.
* If using Apache, point a virtual host to the deploy directory/public.
    * For example. deploy-directory=/var/www/ats, virtual host points to /var/www/ats/public
* Configure the database credentials in application/config/database.php
* On the server, Run `cd $deploy-directory; php artisan migrate:install; php artisan migrate`
* Create the first super administrator with `php artisan initialize:super_admin john@doe.com password1`
* The application should be ready to go.
* Do not forget to document your decisions so that they can be replicated if necessary.

## Using the provided Virtual Machine Appliance
* The appliance can be found on the provided flashdrive or can be downloaded from the links below. (For a very limited time)
    * [64-bit](https://mega.co.nz/#!0sZHhTYI!K9HAYFQ_b65I02ZYq8xe_xUeZfN0WPgNWGB5MInY21s)
    * [32-bit](
* Import the appliance with VMware or Virtualbox.
* The username and password are each `ats`.
* MySQL connection information can be found in application/config/database.php
* The latest code as of 05/01/2013 is already installed in /var/www/ats/current
* This image was created using the automated Ubuntu install below and the Capistrano deploy method.
* [Update the code](#deploy) or Proceed to [initial deploy](#initial-deploy)

## Preparing Ubuntu 12.10 Server Virtual Machine or even a bare Ubuntu install (With Automation)
### This is an advanced deployment option. It makes things very simple but may not work well on Windows.
### Prepare the Server
1. Install [Ubuntu 12.10 Server](http://www.ubuntu.com/download/server) into a Virtual Machine
1. Configure SSH(On VM)
    * `sudo aptitude update`
    * `sudo aptitude install openssh-server`
    * Add your public key to the root user's `authorized_keys` ([This article](https://help.github.com/articles/generating-ssh-keys#platform-windows) demonstrates creating a public key)
        1. Copy your public key to the ats users home directory.
        1. `sudo su` (On VM)
        1. `mkdir /root/.ssh/ && cat id_rsa.pub >> /root/.ssh/authorized_keys` (On VM)
        1. `chmod 600 /root/.ssh/authorized_keys && exit` (On VM)
1. Install [SaltStack](http://saltstack.com/community.html)(On VM)
    * `wget -O - http://bootstrap.saltstack.org | sudo sh`
1. Place the correct ip address or hostname in prepare.sh.(On Host)
1. Run `./prepare.sh` twice (There is a bug in SaltStack that prevents mysql from being configured the first time. (On Host)
    * Something similar will be necessary for Windows.
1. [Deploy](#deploy) the code.

##<a id="deploy"></a> Deploy the Code
### With FTP
1. Copy the latest files to /var/www/ats/current (Change this path if you performed a custom install) on the server.
1. Perform [initial deploy](#initial-deploy) steps if necessary.

### With Capistrano (Advanced)
1. Setup [ruby](http://rubyinstaller.org/downloads/) and [Bundler](http://gembundler.com/) (On Host)
    * This should be possible on Windows.
1. Run `bundle install` in the project directory. (On Host)
1. Edit config/deploy.rb to point to the correct ip address of the deployment server. (On Host)
1. Run `cap deploy:setup` to prepare the server for code deployment. (On Host)
1. Run `cap deploy` to deploy the code to the server. (On Host)
1. Perform [initial deploy](#initial-deploy) steps if necessary.

##<a id="initial-deploy"></a> Post Initial Deploy
### Setup the first Super Administrator for the system
* Run `cd /var/www/ats/current; php artisan initialize:super_admin YOUR_ADMIN@YOUR_EMAIL.com PASSWORD`

## Setup Development Environment _(This was used during the development of the application and should be updated to reflect the current status of development at ATS)_

Access at ipaddress/laravel

1. Download the [laravel_appliance](https://mega.co.nz/#!J4ZgxD6J!TLDpwimotUmfkv4X6FksoBD_X_kT49thgVlv4-abhoI)
2. Import it into VirtualBox or VMWare.
    * The username is bitnami and the password is inventory.
3. Clone this repository to your computer.
    * `git clone git@bitbucket.org:rrobeson/ats_inventory_management_system.git`
4. Share the cloned folder from your host machine to the virtual machine.
5. SSH into the virtual machine with the ipaddress it displays.
    * `ssh bitnami@192.*.*.*`
6. Rename the current laravel folder to something else. (We should not be needing it)
    * `sudo mv /opt/bitnami/frameworks/laravel /opt/bitnami/frameworks/laravel_bak`
7. Link the shared folder on the virtual machine to the appropriate location.
    * `sudo ln -s /mnt/hgfs/FOLDER_PATH /opt/bitnami/frameworks/laravel` - This is possibly VMWare specific. Virtualbox may be different.
8. Reboot the virtual machine.
9. Setup the database (While logged into the virtual machine).
    * `mysql -u root -p` (The password for root mysql is bitnami)
    * mysql> `CREATE DATABASE ats`
10. Change to the laravel directory.
    * `cd /opt/bitnami/frameworks/laravel`
11. Run migrations and things on the virtual machine when we get those going.
    * `php artisan migrate:install`
    * `php artisan migrate`
12. Create some user accounts
    * `php artisan initialize:super_admin sa@gmail.com password`
    * `php artisan initialize:admin a@gmail.com password`
    * `php artisan initialize:read_only_admin ro@gmail.com password`
13. Now changes made on your host machine will be reflected in the virtual machine without worrying about copying files back and forth.
14. Artisan commands should be run from the virtual machine itself (through ssh or simply clicking on the virtual machine window and logging in and running them).
15. Open up the application and login with one of the accounts created above

## Commonly Used Git Commands
* A great git reference can be found at [git-scm.com](http://git-scm.com/book)
* Get the latest changes from bitbucket. `git pull`
* Push changes to bitbucket. `git push`
* View the status of files. `git status`
* Create a new branch from the current one and switch to it. `git checkout -b branch_name`
* Delete a branch (*Do not delete master*). `git branch -d branch_name`
* Change to a branch. `git checkout branch_name`
* Merge a branches changes into the current branch. `git merge branch_name`
* Stage all files for committing. `git add .`
* Stage a single file or list of files for commiting. `git add file_name_one file_name_two`
* Commit staged files. `git commit`
