nginx:
  pkg:
    - installed
  service.running:
    - require:
      - pkg: nginx
      - file: /etc/nginx
    - watch:
      - file: /etc/nginx
      - file: /etc/nginx/sites-enabled/*
git:
  pkg:
    - installed

mysql-server:
  pkg:
    - installed

python-mysqldb:
  pkg:
    - installed
  require:
    - pkg: mysql-server

php5-cli:
  pkg:
    - installed

php5-mysql:
  pkg:
    - installed

php5-mcrypt:
  pkg:
    - installed

php5-fpm:
  pkg:
    - installed
  service.running:
    - require:
      - pkg: php5-fpm
    - watch:
      - file: /etc/nginx

/etc/nginx:
  file.recurse:
    - source: salt://nginx/config
    - user: root
    - group: root
    - file_mode: 644

/etc/nginx/sites-enabled/ats:
  file.symlink:
    - target: /etc/nginx/sites-available/ats
  require:
    - file: /etc/nginx/sites-available/ats

/etc/nginx/sites-enabled/default:
  file.absent

salt-mysql-settings:
  file.append:
    - name: /etc/salt/minion
    - text: "mysql.default_file: /etc/mysql/debian.cnf"

ats-mysql-setup:
  mysql_database.present:
    - name: ats
  mysql_user.present:
    - name: ats_eis
    - password: "faiphaXe4aBaiph4"
  mysql_grants.present:
    - database: ats.*
    - grant: ALL PRIVILEGES
    - user: ats_eis
  require:
    - pkg: python-mysqldb

/var/www:
  file.directory:
    - user: www-data
    - group: www-data
    - makedirs: True
    - recurse:
      - user
      - group
