$(function() {
    // Deal with DataTables
    $('.datatable').each(function() {
        var not_sortable = $(this).attr("data-datatable-not-sortable");
        var not_visible = $(this).attr("data-datatable-not-visible");

        if(not_sortable) {
            not_sortable = not_sortable.split(",");

            for(i=0; i < not_sortable.length; i++) {
                not_sortable[i] = parseInt(not_sortable[i]);
            }
        } else {
            not_sortable = [];
        }

        if(not_visible) {
            not_visible = not_visible.split(",");

            for(i=0; i < not_visible.length; i++) {
                not_visible[i] = parseInt(not_visible[i]);
            }
        } else {
            not_visible = [];
        }

        $(this).dataTable({
            "sDom": 'T<"clear">lfrtip',
            "oTableTools": {
                "sSwfPath": window.sSwfPath
            },
            "aaSorting": [],
            "bSortClasses": false,
            "aoColumnDefs": [
                { "bSortable": false, "aTargets": not_sortable },
                { "bVisible": false, "aTargets": not_visible }
            ]
        });
    });
    // End of DataTables
});
