<?php
    
    class Data_Task {
        public function run($arguments) {
            $cameras = new ItemCategory;
            $cameras->name = 'Cameras';
            $cameras->description = 'Cameras';
            $cameras->save();

            $projectors = new ItemCategory;
            $projectors->name = 'Projectors';
            $projectors->description = 'Projectors';
            $projectors->save();

            $tablets = new ItemCategory;
            $tablets->name = 'Tablets, eReaders, & Mobile Devices';
            $tablets->description = 'Tablets, eReaders, & Mobile Devices';
            $tablets->save();

            $laptops = new ItemCategory;
            $laptops->name = 'Laptops';
            $laptops->description = 'Laptops';
            $laptops->save();

            $microphones = new ItemCategory;
            $microphones->name = 'Microphones & Recorders';
            $microphones->description = 'Microphones & Recorders';
            $microphones->save();

            $pc = new ItemCategory;
            $pc->name = 'PC Peripherals';
            $pc->description = 'PC Peripherals';
            $pc->save();

            $response = new ItemCategory;
            $response->name = 'Response Systems';
            $response->description = 'Response Systems';
            $response->save();
            
            $item = new Item;
            $item->inventory_number = '223456';  
            $item->name = 'iPod Touch';   
            $item->monetary_value = 199.00;         
            $item->category = $tablets->id; 
            $item->keywords = 'iPod Touch';
            $item->allowed_checkout_length = 7;
            $item->description = 'iPod Touch';
            $item->comments = 'iPod Touch';
            $item->status = 'Good';
            $item->availability = 1;
            $item->serial_number = 'X223456';
            $item->save();
            
            $item = new Item;
            $item->inventory_number = '223457';  
            $item->name = 'iPod Touch';   
            $item->monetary_value = 299.00;         
            $item->category = $tablets->id; 
            $item->keywords = 'iPod Touch';
            $item->allowed_checkout_length = 7;
            $item->description = 'iPod Touch';
            $item->comments = 'iPod Touch';
            $item->status = 'damaged';
            $item->availability = 0;
            $item->serial_number = 'X223457';
            $item->save();
            
            $item = new Item;
            $item->inventory_number = '223458';  
            $item->name = 'iPod Touch';   
            $item->monetary_value = 399.00;         
            $item->category = $tablets->id; 
            $item->keywords = 'iPod Touch';
            $item->allowed_checkout_length = 7;
            $item->description = 'iPod Touch';
            $item->comments = 'iPod Touch';
            $item->status = 'surplus';
            $item->availability = 0;
            $item->serial_number = 'X223458';
            $item->save();
            
            $item = new Item;
            $item->inventory_number = '223459';  
            $item->name = 'iPod Touch';   
            $item->monetary_value = 199.00;         
            $item->category = $tablets->id; 
            $item->keywords = 'iPod Touch';
            $item->allowed_checkout_length = 14;
            $item->description = 'iPod Touch';
            $item->comments = 'iPod Touch';
            $item->status = 'stolen';
            $item->availability = 0;
            $item->serial_number = 'X223459';
            $item->save();
            
            $item = new Item;
            $item->inventory_number = '223460';  
            $item->name = 'iPod Touch';   
            $item->monetary_value = 199.00;         
            $item->category = $tablets->id; 
            $item->keywords = 'iPod Touch';
            $item->allowed_checkout_length = 14;
            $item->description = 'iPod Touch';
            $item->comments = 'iPod Touch';
            $item->status = 'lost';
            $item->availability = 0;
            $item->serial_number = 'X223460';
            $item->save();
            
            $item = new Item;
            $item->inventory_number = '223461';  
            $item->name = 'iPad III';   
            $item->monetary_value = 399.00;         
            $item->category = $tablets->id; 
            $item->keywords = 'iPad III';
            $item->allowed_checkout_length = 14;
            $item->description = 'iPad III';
            $item->comments = 'iPad III';
            $item->status = 'good';
            $item->availability = 1;
            $item->serial_number = 'X223461';
            $item->save();
            
            $item = new Item;
            $item->inventory_number = '223462';  
            $item->name = 'iPad III';   
            $item->monetary_value = 499.00;         
            $item->category = $tablets->id; 
            $item->keywords = 'iPad III';
            $item->allowed_checkout_length = 14;
            $item->description = 'iPad III';
            $item->comments = 'iPad III';
            $item->status = 'good';
            $item->availability = 1;
            $item->serial_number = 'X223462';
            $item->save();
            
            $item = new Item;
            $item->inventory_number = '223463';  
            $item->name = 'iPad III';   
            $item->monetary_value = 599.00;         
            $item->category = $tablets->id; 
            $item->keywords = 'iPad III';
            $item->allowed_checkout_length = 14;
            $item->description = 'iPad III';
            $item->comments = 'iPad III';
            $item->status = 'good';
            $item->availability = 1;
            $item->serial_number = 'X223463';
            $item->save();
            
            $item = new Item;
            $item->inventory_number = '223464';  
            $item->name = 'iPad III';   
            $item->monetary_value = 699.00;         
            $item->category = $tablets->id; 
            $item->keywords = 'iPad III';
            $item->allowed_checkout_length = 14;
            $item->description = 'iPad III';
            $item->comments = 'iPad III';
            $item->status = 'good';
            $item->availability = 1;
            $item->serial_number = 'X223464';
            $item->save();
            
            $item = new Item;
            $item->inventory_number = '223465';  
            $item->name = 'Flip Camera';   
            $item->monetary_value = 299.00;         
            $item->category = $cameras->id; 
            $item->keywords = 'Flip Camera';
            $item->allowed_checkout_length = 14;
            $item->description = 'Flip Camera';
            $item->comments = 'Flip Camera';
            $item->status = 'good';
            $item->availability = 1;
            $item->serial_number = 'X223465';
            $item->save();
            
            $item = new Item;
            $item->inventory_number = '223466';  
            $item->name = 'Flip Camera';   
            $item->monetary_value = 299.00;         
            $item->category = $cameras->id; 
            $item->keywords = 'Flip Camera';
            $item->allowed_checkout_length = 14;
            $item->description = 'Flip Camera';
            $item->comments = 'Flip Camera';
            $item->status = 'good';
            $item->availability = 1;
            $item->serial_number = 'X223466';
            $item->save();
            
            $item = new Item;
            $item->inventory_number = '223467';  
            $item->name = 'Flip Camera';   
            $item->monetary_value = 299.00;         
            $item->category = $cameras->id; 
            $item->keywords = 'Flip Camera';
            $item->allowed_checkout_length = 14;
            $item->description = 'Flip Camera';
            $item->comments = 'Flip Camera';
            $item->status = 'good';
            $item->availability = 1;
            $item->serial_number = 'X223467';
            $item->save();
            
            $item = new Item;
            $item->inventory_number = '223468';  
            $item->name = 'Flip Camera';   
            $item->monetary_value = 299.00;         
            $item->category = $cameras->id; 
            $item->keywords = 'Flip Camera';
            $item->allowed_checkout_length = 14;
            $item->description = 'Flip Camera';
            $item->comments = 'Flip Camera';
            $item->status = 'damaged';
            $item->availability = 0;
            $item->serial_number = 'X223468';
            $item->save();
            
            $item = new Item;
            $item->inventory_number = '223469';  
            $item->name = 'Amazon Kindle';   
            $item->monetary_value = 179.00;         
            $item->category = $tablets->id; 
            $item->keywords = 'Amazon Kindle';
            $item->allowed_checkout_length = 14;
            $item->description = 'Amazon Kindle';
            $item->comments = 'Amazon Kindle';
            $item->status = 'damaged';
            $item->availability = 0;
            $item->serial_number = 'X223469';
            $item->save();
            
            $item = new Item;
            $item->inventory_number = '223470';  
            $item->name = 'Amazon Kindle';   
            $item->monetary_value = 179.00;         
            $item->category = $tablets->id; 
            $item->keywords = 'Amazon Kindle';
            $item->allowed_checkout_length = 7;
            $item->description = 'Amazon Kindle';
            $item->comments = 'Amazon Kindle';
            $item->status = 'good';
            $item->availability = 1;
            $item->serial_number = 'X223470';
            $item->save();
            
            $item = new Item;
            $item->inventory_number = '223471';  
            $item->name = 'Amazon Kindle';   
            $item->monetary_value = 179.00;         
            $item->category = $tablets->id; 
            $item->keywords = 'Amazon Kindle';
            $item->allowed_checkout_length = 7;
            $item->description = 'Amazon Kindle';
            $item->comments = 'Amazon Kindle';
            $item->status = 'good';
            $item->availability = 1;
            $item->serial_number = 'X223471';
            $item->save();
            
            $item = new Item;
            $item->inventory_number = '223472';  
            $item->name = 'Amazon Kindle';   
            $item->monetary_value = 179.00;         
            $item->category = $tablets->id; 
            $item->keywords = 'Amazon Kindle';
            $item->allowed_checkout_length = 7;
            $item->description = 'Amazon Kindle';
            $item->comments = 'Amazon Kindle';
            $item->status = 'good';
            $item->availability = 1;
            $item->serial_number = 'X223472';
            $item->save();
            
            $item = new Item;
            $item->inventory_number = '223473';  
            $item->name = 'Turning Point Response System';   
            $item->monetary_value = 179.00;         
            $item->category = $response->id; 
            $item->keywords = 'Turning Point Response System';
            $item->allowed_checkout_length = 14;
            $item->description = 'Turning Point Response System';
            $item->comments = 'Turning Point Response System';
            $item->status = 'good';
            $item->availability = 1;
            $item->serial_number = 'X223473';
            $item->save();
            
            $item = new Item;
            $item->inventory_number = '223474';  
            $item->name = 'Turning Point Response System';   
            $item->monetary_value = 279.00;         
            $item->category = $response->id; 
            $item->keywords = 'Turning Point Response System';
            $item->allowed_checkout_length = 14;
            $item->description = 'Turning Point Response System';
            $item->comments = 'Turning Point Response System';
            $item->status = 'good';
            $item->availability = 1;
            $item->serial_number = 'X223474';
            $item->save();
            
            $item = new Item;
            $item->inventory_number = '223475';  
            $item->name = 'Turning Point Response System';   
            $item->monetary_value = 279.00;         
            $item->category = $response->id; 
            $item->keywords = 'Turning Point Response System';
            $item->allowed_checkout_length = 14;
            $item->description = 'Turning Point Response System';
            $item->comments = 'Turning Point Response System';
            $item->status = 'surplus';
            $item->availability = 0;
            $item->serial_number = 'X223475';
            $item->save();
            
            $item = new Item;
            $item->inventory_number = '223476';  
            $item->name = 'NEC VT770';   
            $item->monetary_value = 579.00;         
            $item->category = $projectors->id; 
            $item->keywords = 'Projectors NEC VT770';
            $item->allowed_checkout_length = 14;
            $item->description = 'NEC VT770';
            $item->comments = 'NEC VT770';
            $item->status = 'stolen';
            $item->availability = 0;
            $item->serial_number = 'X223476';
            $item->save();
            
            $item = new Item;
            $item->inventory_number = '223477';  
            $item->name = 'NEC VT770';   
            $item->monetary_value = 579.00;         
            $item->category = $projectors->id; 
            $item->keywords = 'Projectors NEC VT770';
            $item->allowed_checkout_length = 7;
            $item->description = 'NEC VT770';
            $item->comments = 'NEC VT770';
            $item->status = 'good';
            $item->availability = 1;
            $item->serial_number = 'X223477';
            $item->save();
            
            $item = new Item;
            $item->inventory_number = '223478';  
            $item->name = 'NEC VT770';   
            $item->monetary_value = 579.00;         
            $item->category = $projectors->id; 
            $item->keywords = 'Projectors NEC VT770';
            $item->allowed_checkout_length = 7;
            $item->description = 'NEC VT770';
            $item->comments = 'NEC VT770';
            $item->status = 'good';
            $item->availability = 1;
            $item->serial_number = 'X223478';
            $item->save();
            
            $item = new Item;
            $item->inventory_number = '223479';  
            $item->name = 'Lexar Card Reader';   
            $item->monetary_value = 99.00;         
            $item->category = $pc->id; 
            $item->keywords = 'Lexar Card Reader';
            $item->allowed_checkout_length = 14;
            $item->description = 'Lexar Card Reader';
            $item->comments = 'Lexar Card Reader';
            $item->status = 'good';
            $item->availability = 1;
            $item->serial_number = 'X223479';
            $item->save();
            
            $item = new Item;
            $item->inventory_number = '223480';  
            $item->name = 'Lexar Card Reader';   
            $item->monetary_value = 99.00;         
            $item->category = $pc->id; 
            $item->keywords = 'Lexar Card Reader';
            $item->allowed_checkout_length = 14;
            $item->description = 'Lexar Card Reader';
            $item->comments = 'Lexar Card Reader';
            $item->status = 'good';
            $item->availability = 1;
            $item->serial_number = 'X223480';
            $item->save();
            
            $item = new Item;
            $item->inventory_number = '223481';  
            $item->name = 'Lexar Card Reader';   
            $item->monetary_value = 99.00;         
            $item->category = $pc->id; 
            $item->keywords = 'Lexar Card Reader';
            $item->allowed_checkout_length = 14;
            $item->description = 'Lexar Card Reader';
            $item->comments = 'Lexar Card Reader';
            $item->status = 'good';
            $item->availability = 1;
            $item->serial_number = 'X223481';
            $item->save();
            
            $item = new Item;
            $item->inventory_number = '223482';  
            $item->name = 'Zoom Hz Recorder';   
            $item->monetary_value = 99.00;         
            $item->category = $microphones->id; 
            $item->keywords = 'Zoom Hz Recorder';
            $item->allowed_checkout_length = 14;
            $item->description = 'Zoom Hz Recorder';
            $item->comments = 'Zoom Hz Recorder';
            $item->status = 'good';
            $item->availability = 1;
            $item->serial_number = 'X223482';
            $item->save();
            
            $item = new Item;
            $item->inventory_number = '223483';  
            $item->name = 'Zoom Hz Recorder';   
            $item->monetary_value = 99.00;         
            $item->category = $microphones->id; 
            $item->keywords = 'Zoom Hz Recorder';
            $item->allowed_checkout_length = 14;
            $item->description = 'Zoom Hz Recorder';
            $item->comments = 'Zoom Hz Recorder';
            $item->status = 'good';
            $item->availability = 1;
            $item->serial_number = 'X223483';
            $item->save();
            
            $item = new Item;
            $item->inventory_number = '223484';  
            $item->name = 'Zoom Hz Recorder';   
            $item->monetary_value = 99.00;         
            $item->category = $microphones->id; 
            $item->keywords = 'Zoom Hz Recorder';
            $item->allowed_checkout_length = 14;
            $item->description = 'Zoom Hz Recorder';
            $item->comments = 'Zoom Hz Recorder';
            $item->status = 'good';
            $item->availability = 1;
            $item->serial_number = 'X223484';
            $item->save();
            
            $item = new Item;
            $item->inventory_number = '223485';  
            $item->name = 'Macbook Pro';   
            $item->monetary_value = 1499.00;         
            $item->category = $laptops->id; 
            $item->keywords = 'Macbook Pro';
            $item->allowed_checkout_length = 14;
            $item->description = 'Macbook Pro';
            $item->comments = 'Macbook Pro';
            $item->status = 'good';
            $item->availability = 1;
            $item->serial_number = 'X223485';
            $item->save();
            
            $item = new Item;
            $item->inventory_number = '223486';  
            $item->name = 'Macbook Pro';   
            $item->monetary_value = 1499.00;         
            $item->category = $laptops->id; 
            $item->keywords = 'Macbook Pro';
            $item->allowed_checkout_length = 7;
            $item->description = 'Macbook Pro';
            $item->comments = 'Macbook Pro';
            $item->status = 'good';
            $item->availability = 1;
            $item->serial_number = 'X223486';
            $item->save();
            
            $item = new Item;
            $item->inventory_number = '223487';  
            $item->name = 'Macbook Pro';   
            $item->monetary_value = 1499.00;         
            $item->category = $laptops->id; 
            $item->keywords = 'Macbook Pro';
            $item->allowed_checkout_length = 7;
            $item->description = 'Macbook Pro';
            $item->comments = 'Macbook Pro';
            $item->status = 'good';
            $item->availability = 1;
            $item->serial_number = 'X223487';
            $item->save();
            
            $item = new Item;
            $item->inventory_number = '223488';  
            $item->name = 'Macbook Pro';   
            $item->monetary_value = 1499.00;         
            $item->category = $laptops->id; 
            $item->keywords = 'Macbook Pro';
            $item->allowed_checkout_length = 7;
            $item->description = 'Macbook Pro';
            $item->comments = 'Macbook Pro';
            $item->status = 'damaged';
            $item->availability = 0;
            $item->serial_number = 'X223488';
            $item->save();
            
            $item = new Item;
            $item->inventory_number = '223489';  
            $item->name = 'Dell Laptop';   
            $item->monetary_value = 699.00;         
            $item->category = $laptops->id; 
            $item->keywords = 'Dell Laptop';
            $item->allowed_checkout_length = 14;
            $item->description = 'Dell Laptop';
            $item->comments = 'Dell Laptop';
            $item->status = 'damaged';
            $item->availability = 0;
            $item->serial_number = 'X223489';
            $item->save();
            
            $item = new Item;
            $item->inventory_number = '223490';  
            $item->name = 'Dell Laptop';   
            $item->monetary_value = 699.00;         
            $item->category = $laptops->id; 
            $item->keywords = 'Dell Laptop';
            $item->allowed_checkout_length = 14;
            $item->description = 'Dell Laptop';
            $item->comments = 'Dell Laptop';
            $item->status = 'good';
            $item->availability = 1;
            $item->serial_number = 'X223490';
            $item->save();
            
            $item = new Item;
            $item->inventory_number = '223491';  
            $item->name = 'Dell Laptop';   
            $item->monetary_value = 699.00;         
            $item->category = $laptops->id; 
            $item->keywords = 'Dell Laptop';
            $item->allowed_checkout_length = 14;
            $item->description = 'Dell Laptop';
            $item->comments = 'Dell Laptop';
            $item->status = 'good';
            $item->availability = 1;
            $item->serial_number = 'X223491';
            $item->save();
            
            $item = new Item;
            $item->inventory_number = '223492';  
            $item->name = 'Dell Laptop';   
            $item->monetary_value = 699.00;         
            $item->category = $laptops->id; 
            $item->keywords = 'Dell Laptop';
            $item->allowed_checkout_length = 14;
            $item->description = 'Dell Laptop';
            $item->comments = 'Dell Laptop';
            $item->status = 'good';
            $item->availability = 1;
            $item->serial_number = 'X223492';
            $item->save();
        }
		
		public function faculty($arguments) {
		    
			$faculty = new Faculty;
			$faculty->first_name = 'George';
			$faculty->last_name = 'Aktinson';
			$faculty->faculty_id = 'E20000002';
			$faculty->email_address = 'george.aktinson@gmail.com';
			$faculty->department = 'MUSC';
			$faculty->phone_number = '4235555555';
			$faculty->save();
			
			$faculty = new Faculty;
			$faculty->first_name = 'Wendy';
			$faculty->last_name = 'Stalls';
			$faculty->faculty_id = 'E20000003';
			$faculty->email_address = 'wendy.stalls@gmail.com';
			$faculty->department = 'CSCI';
			$faculty->phone_number = '4235555555';
			$faculty->save();
			
			$faculty = new Faculty;
			$faculty->first_name = 'Mathew';
			$faculty->last_name = 'Parks';
			$faculty->faculty_id = 'E20000004';
			$faculty->email_address = 'mathew.parks@gmail.com';
			$faculty->department = 'BIOL';
			$faculty->phone_number = '4235555555';
			$faculty->save();
			
			$faculty = new Faculty;
			$faculty->first_name = 'Sally';
			$faculty->last_name = 'Fields';
			$faculty->faculty_id = 'E20000005';
			$faculty->email_address = 'sally.fields@gmail.com';
			$faculty->department = 'FINE';
			$faculty->phone_number = '4235555555';
			$faculty->save();
			
			$faculty = new Faculty;
			$faculty->first_name = 'Dave';
			$faculty->last_name = 'Markson';
			$faculty->faculty_id = 'E20000006';
			$faculty->email_address = 'dave.markson@gmail.com';
			$faculty->department = 'PHYS';
			$faculty->phone_number = '4235555555';
			$faculty->save();
			
			$faculty = new Faculty;
			$faculty->first_name = 'Kim';
			$faculty->last_name = 'Kendall';
			$faculty->faculty_id = 'E20000007';
			$faculty->email_address = 'kim.kendall@gmail.com';
			$faculty->department = 'ARTS';
			$faculty->phone_number = '4235555555';
			$faculty->save();
			
			$faculty = new Faculty;
			$faculty->first_name = 'Phil';
			$faculty->last_name = 'Johnson';
			$faculty->faculty_id = 'E20000008';
			$faculty->email_address = 'phil.johnson@gmail.com';
			$faculty->department = 'MATH';
			$faculty->phone_number = '4235555555';
			$faculty->save();
			
			$faculty = new Faculty;
			$faculty->first_name = 'Bob';
			$faculty->last_name = 'Greco';
			$faculty->faculty_id = 'E20000009';
			$faculty->email_address = 'bob.greco@gmail.com';
			$faculty->department = 'PHSY';
			$faculty->phone_number = '4235555555';
			$faculty->save();
						
		}
		
		public function checked_out_transactions($arguments) {
            $faculty = Faculty::all();
            $users = User::all();
            $items = Item::all();

            $faculty_ids = [];
            foreach ($faculty as $f)
            {
                $faculty_ids[] = $f->id;
            }

            $user_ids = [];
            foreach ($users as $u)
            {
                $user_ids[] = $u->id;
            }

            $item_ids = [];
            foreach ($items as $i)
            {
                $item_ids[] = $i->id;
            }

            shuffle($item_ids);


            for ($i = 0; $i < 20; $i++) {
                $cd = strtotime('-'.mt_rand(0,7).' days');
                $dd = strtotime('+ 7 days', $cd);
                $cd = date('Y-m-d', $cd);
                $dd = date('Y-m-d', $dd);

                $to = $faculty_ids[array_rand($faculty_ids)];
                $by = $user_ids[array_rand($user_ids)];
                $item = array_pop($item_ids);

                $checked_out_transactions = new CheckedOutTransaction;
                $checked_out_transactions->checked_out_date = $cd;
                $checked_out_transactions->due_date = $dd;
                $checked_out_transactions->checked_out_to = $to;
                $checked_out_transactions->checked_out_by = $by;
                $checked_out_transactions->item_checked_out = $item;
                $checked_out_transactions->save();

                $item_to_update = Item::find($item);
                $item_to_update->availability = false;
                $item_to_update->save();
            }
		}
		
    }
?>
