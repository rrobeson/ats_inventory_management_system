<?php
    Bundle::start('sentry');
    Session::load();

    class Initialize_Task {
        public function run($arguments) {
            echo "Usage: php artisan initialize:super_admin EMAIL PASSWORD";
            echo "\n";
            echo "                             :admin EMAIL PASSWORD";
            echo "\n";
            echo "                             :read_only EMAIL PASSWORD";
            echo "\n";
            echo "* super_admin deletes any user with the given email and recreates them as a super admin.";
            echo "\n";
            echo "* admin deletes any user with the given email and recreates them as an admin.";
            echo "\n";
            echo "* read_only_admin deletes any user with the given email and recreates them as a read only admin.";
            echo "\n";
        }

        # Just an example of how to validate permissions of a user
        public function test($arguments) {
            if(json_decode(Sentry::user('ryan.robeson@gmail.com')->permissions(), true)['is_super_admin']) {
                echo "Is super admin";
                echo "\n";
            }

            if(json_decode(Sentry::user('ryan.robeson@gmail.com')->permissions(), true)['is_admin']) {
                echo "Is admin";
                echo "\n";
            }
            if(json_decode(Sentry::user('ryan.robeson@gmail.com')->permissions(), true)['is_read_only_admin']) {
                echo "Is read only admin";
                echo "\n";
            }

            if(!json_decode(Sentry::user('ryan.robeson@gmail.com')->permissions(), true)['is_some_admin']) {
                echo "Is not some admin";
                echo "\n";
            }
        }

        public function super_admin($arguments) {
            # Can't use argument defaults because of the way laravel passes task arguments
            # as an array regardless.
            $super_admin = 'sa@gmail.com';
            $password = 'password';

            # Replace the defaults if there are arguments passed
            if (0 < sizeof($arguments)) {
                if ('' != $arguments[0]) {
                    $super_admin = $arguments[0];
                }

                if ('' != $arguments[1]) {
                    $password = $arguments[1];
                }

            }

            # Delete the user if they already exist
            try {
                Sentry::user($super_admin)->delete();
            } catch (SentryUserNotFoundException $e) {
                // Ignore not finding the user. Doesn't matter if they weren't there to begin with.
            }catch (Exception $e) {
            }

            echo 'Creating super admin: "' . $super_admin . '" with password "' . $password . '"...';
            echo "\n";

            $user = Sentry::user()->create(array(
                'email' => $super_admin,
                'password' => $password,
                'permissions' => array('is_super_admin' => 1, 'is_admin' => 1, 'is_read_only_admin' => 1, 'is_some_admin' => 0),
                'first_name' => 'Super',
                'last_name' => 'Admin',
                'userid' => 'E00000001'

            ));
            
            echo "Done.";
            echo "\n";
        }

        public function admin($arguments) {
            # Can't use argument defaults because of the way laravel passes task arguments
            # as an array regardless.
            $admin = 'a@gmail.com';
            $password = 'password';

            # Replace the defaults if there are arguments passed
            if (0 < sizeof($arguments)) {
                if ('' != $arguments[0]) {
                    $admin = $arguments[0];
                }

                if ('' != $arguments[1]) {
                    $password = $arguments[1];
                }

            }

            # Delete the user if they already exist
            try {
                Sentry::user($admin)->delete();
            } catch (SentryUserNotFoundException $e) {
                // Ignore not finding the user. Doesn't matter if they weren't there to begin with.
            } catch (Exception $e) {
            }

            echo 'Creating admin: "' . $admin . '" with password "' . $password . '"...';
            echo "\n";

            $user = Sentry::user()->create(array(
                'email' => $admin,
                'password' => $password,
                'permissions' => array('is_super_admin' => 0, 'is_admin' => 1, 'is_read_only_admin' => 1, 'is_some_admin' => 0),
                'first_name' => 'Regular',
                'last_name' => 'Admin',
                'userid' => 'E00000002'
            ));
            
            echo "Done.";
            echo "\n";
        }

        public function read_only_admin($arguments) {
            # Can't use argument defaults because of the way laravel passes task arguments
            # as an array regardless.
            $read_only_admin = 'ro@gmail.com';
            $password = 'password';

            # Replace the defaults if there are arguments passed
            if (0 < sizeof($arguments)) {
                if ('' != $arguments[0]) {
                    $read_only_admin = $arguments[0];
                }

                if ('' != $arguments[1]) {
                    $password = $arguments[1];
                }

            }

            # Delete the user if they already exist
            try {
                Sentry::user($read_only_admin)->delete();
            } catch (SentryUserNotFoundException $e) {
                // Ignore not finding the user. Doesn't matter if they weren't there to begin with.
            }catch (Exception $e) {
            }

            echo 'Creating read only admin: "' . $read_only_admin . '" with password "' . $password . '"...';
            echo "\n";

            $user = Sentry::user()->create(array(
                'email' => $read_only_admin,
                'password' => $password,
                'permissions' => array('is_super_admin' => 0, 'is_admin' => 0, 'is_read_only_admin' => 1, 'is_some_admin' => 0),
                'first_name' => 'Read Only',
                'last_name' => 'Admin',
                'userid' => 'E00000003'

            ));
            
            echo "Done.";
            echo "\n";
        }
    }
?>
