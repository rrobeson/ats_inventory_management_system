<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
*/

# Display error if user is not logged in
Route::filter('logged_in', function() {
    if(!Sentry::check()) {
        return Controller::call('home@index');
    }
});

# Should always come after logged_in filter
# Redirect to admin/index if user is not read only admin
Route::filter('read_only_admin_only', function() {
    if(!User::where_email(Sentry::user()->email)->first()->is_read_only_admin()) {
        return Redirect::to('admin/index');
    }
});

# Should always come after logged_in filter
# Redirect to admin/index if user is not admin
Route::filter('admin_only', function() {
    if(!User::where_email(Sentry::user()->email)->first()->is_admin()) {
        return Redirect::to('admin/index');
    }
});

# Should always come after logged_in filter
# Redirect to admin/index if user is not super admin
Route::filter('super_admin_only', function() {
    if(!User::where_email(Sentry::user()->email)->first()->is_super_admin()) {
        return Redirect::to('admin/index');
    }
});

Route::controller('home');
Route::controller('admin');
Route::controller('users');
Route::controller('item_categories');
Route::controller('items');
Route::controller('view');
Route::controller('reports');
Route::controller('holds');
Route::controller('faculty');

/*
|--------------------------------------------------------------------------
| Application 404 & 500 Error Handlers
|--------------------------------------------------------------------------
|
| To centralize and simplify 404 handling, Laravel uses an awesome event
| system to retrieve the response. Feel free to modify this function to
| your tastes and the needs of your application.
|
| Similarly, we use an event to handle the display of 500 level errors
| within the application. These errors are fired when there is an
| uncaught exception thrown in the application.
|
*/

Event::listen('404', function()
{
	return Response::error('404');
});

Event::listen('500', function()
{
	return Response::error('500');
});

/*
|--------------------------------------------------------------------------
| Route Filters
|--------------------------------------------------------------------------
|
| Filters provide a convenient method for attaching functionality to your
| routes. The built-in before and after filters are called before and
| after every request to your application, and you may even create
| other filters that can be attached to individual routes.
|
| Let's walk through an example...
|
| First, define a filter:
|
|		Route::filter('filter', function()
|		{
|			return 'Filtered!';
|		});
|
| Next, attach the filter to a route:
|
|		Router::register('GET /', array('before' => 'filter', function()
|		{
|			return 'Hello World!';
|		}));
|
*/

Route::filter('before', function()
{
	// Do stuff before every request to your application...
});

Route::filter('after', function($response)
{
	// Do stuff after every request to your application...
});

Route::filter('csrf', function()
{
	if (Request::forged()) return Response::error('500');
});

Route::filter('auth', function()
{
	if (Auth::guest()) return Redirect::to('login');
});
