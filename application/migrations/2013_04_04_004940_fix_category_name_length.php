<?php

class Fix_Category_Name_Length {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
        DB::query('alter table item_categories modify name varchar(100)');
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
        DB::query('alter table item_categories modify name varchar(32)');
	}

}
