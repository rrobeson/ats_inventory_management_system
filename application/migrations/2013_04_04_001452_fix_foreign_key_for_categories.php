<?php

class Fix_Foreign_Key_For_Categories {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('items', function($table)
        {
            $table->drop_foreign('items_category_foreign');
            $table->drop_column('category');
        });
        Schema::table('items', function($table)
        {
            $table->integer('category')->unsigned();
            $table->foreign('category')->references('id')->on('item_categories');
        });
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('items', function($table)
        {
            $table->drop_foreign('items_category_foreign');
            $table->drop_column('category');
        });
        Schema::table('items', function($table)
        {
            $table->string('category', 32);
            $table->foreign('category')->references('name')->on('item_categories');
        });
	}

}
