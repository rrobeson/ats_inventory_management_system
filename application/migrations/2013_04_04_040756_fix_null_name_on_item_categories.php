<?php

class Fix_Null_Name_On_Item_Categories {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
        DB::query('alter table item_categories modify name varchar(100) not null');
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
        DB::query('alter table item_categories modify name varchar(100)');
	}

}
