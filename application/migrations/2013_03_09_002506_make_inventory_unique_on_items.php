<?php

class Make_Inventory_Unique_On_Items {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('items', function($table)
        {
            $table->unique('inventory_number');
        });
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('items', function($table)
        {
            $table->drop_unique('items_inventory_number_unique');
        });
	}

}
