<?php

class Change_Fields_To_Nullable_On_Holds {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
        DB::query('alter table holds modify approved_or_denied_by int(10) unsigned');
        DB::query('alter table holds modify status varchar(200) null');
        DB::query('alter table holds modify date_approved timestamp null');
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
        DB::query('alter table holds modify approved_or_denied_by int(10) unsigned not null');
        DB::query('alter table holds modify date_approved timestamp not null');
        DB::query('alter table holds modify status varchar(200) not null');
	}

}
