<?php

class Change_Faculty_Barcode_To_Nullable {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
        DB::query('alter table faculty modify barcode_number varchar(32) null');
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
        DB::query('alter table faculty modify barcode_number varchar(32) not null');
	}

}
