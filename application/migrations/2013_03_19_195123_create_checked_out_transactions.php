<?php

class Create_Checked_Out_Transactions {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('users', function($table)
        {
            $table->unique('userid');
        });
        Schema::table('checked_out_transactions', function($table)
        {
            $table->create();
            $table->increments('id');
            $table->timestamp('checked_out_date');
            $table->timestamp('checked_in_date')->nullable();
            $table->timestamp('due_date');
            $table->timestamp('renewal_due_date')->nullable();
            $table->integer('checked_out_to')->unsigned();
            $table->foreign('checked_out_to')->references('id')->on('faculty');	
            $table->integer('checked_out_by')->unsigned();
            $table->foreign('checked_out_by')->references('id')->on('users');		
            $table->integer('checked_in_by')->unsigned()->nullable();
            $table->foreign('checked_in_by')->references('id')->on('users');		
            $table->integer('item_checked_out')->unsigned();
            $table->foreign('item_checked_out')->references('id')->on('items');
            $table->integer('renewal_checked_out_by')->unsigned()->nullable();	
            $table->foreign('renewal_checked_out_by')->references('id')->on('users');	
            $table->engine = 'InnoDB';
        });
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('checked_out_transactions');
        Schema::table('users', function($table) {
            $table->drop_unique('users_userid_unique');
        });
	}

}
