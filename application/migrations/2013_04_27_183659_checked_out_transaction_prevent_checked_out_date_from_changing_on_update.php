<?php

class Checked_Out_Transaction_Prevent_Checked_Out_Date_From_Changing_On_Update {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
        DB::query('alter table checked_out_transactions change checked_out_date checked_out_date timestamp not null default current_timestamp');
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
        DB::query('alter table checked_out_transactions change checked_out_date checked_out_date timestamp not null default current_timestamp on update current_timestamp');
	}

}
