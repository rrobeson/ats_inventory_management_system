<?php

class Create_Holds {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('holds', function($table)
        {
            $table->create();
            $table->increments('id');
            $table->timestamp('start_date');
            $table->timestamp('end_date');
            $table->integer('held_for')->unsigned();
            $table->foreign('held_for')->references('id')->on('faculty');		
            $table->integer('approved_or_denied_by')->unsigned();
            $table->foreign('approved_or_denied_by')->references('id')->on('users');			
            $table->string('status');
            $table->timestamp('date_approved');
            $table->integer('item')->unsigned();
            $table->foreign('item')->references('id')->on('items');				
            $table->engine = 'InnoDB';
        });
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('holds');
	}

}
