<?php

class Add_Fields_To_Users {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('users', function($table) {
            $table->string('userid');
            $table->string('first_name');
            $table->string('last_name');
        });
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('users', function($table) {
            $table->drop_column('userid');
            $table->drop_column('first_name');
            $table->drop_column('last_name');
        });
	}

}
