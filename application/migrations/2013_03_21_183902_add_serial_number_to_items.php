<?php

class Add_Serial_Number_To_Items {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('items', function($table)
        {
            $table->string('serial_number')->nullable();
        });
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('items', function($table)
        {
            $table->drop_column('serial_number');
        });
	}

}
