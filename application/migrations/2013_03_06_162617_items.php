<?php

class Items {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('items', function($table)
        {
            $table->create();
            $table->string('inventory_number', 32)->primary();
            $table->string('name', 32);
            $table->decimal('monetary_value', 10, 2);
            $table->string('category', 32);
            $table->foreign('category')->references('name')->on('item_categories');
            $table->text('keywords');
            $table->integer('allowed_checkout_length');
            $table->text('description');
            $table->text('comments');
            $table->string('status', 32);
            $table->boolean('availability');
            $table->timestamps();
            $table->engine = 'InnoDB';
        });
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('items');
	}

}
