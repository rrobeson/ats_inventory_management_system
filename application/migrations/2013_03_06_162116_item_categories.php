<?php

class Item_Categories {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('item_categories', function($table)
        {
            $table->create();
            $table->increments('id');
            $table->string('name', 32)->unique();
            $table->text('description');
            $table->engine = 'InnoDB';
        });
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('item_categories');
	}

}
