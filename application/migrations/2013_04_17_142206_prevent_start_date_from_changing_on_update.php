<?php

class Prevent_Start_Date_From_Changing_On_Update {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
        DB::query('alter table holds change start_date start_date timestamp not null default current_timestamp');
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
        DB::query('alter table holds change start_date start_date timestamp not null default current_timestamp on update current_timestamp');
	}

}
