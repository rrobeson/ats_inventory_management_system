<?php

class Add_Updated_At_And_Created_At_To_Checked_Out_Transactions {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('checked_out_transactions', function($table)
        {
            $table->timestamps();
        });
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('checked_out_transactions', function($table)
        {
            $table->drop_column(array('updated_at', 'created_at'));
        });
	}

}
