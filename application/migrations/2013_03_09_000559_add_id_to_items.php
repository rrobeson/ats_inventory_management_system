<?php

class Add_Id_To_Items {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('items', function($table)
        {
            $table->drop_primary('items_inventory_number_primary');
        });
        Schema::table('items', function($table)
        {
            $table->increments('id');
        });
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('items', function($table)
        {
            $table->drop_column('id');
        });
        Schema::table('items', function($table)
        {
            $table->primary('inventory_number');
        });
	}

}
