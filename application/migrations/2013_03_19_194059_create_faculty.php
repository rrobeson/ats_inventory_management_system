<?php

class Create_Faculty {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('faculty', function($table)
        {
            $table->create();
            $table->increments('id');
            $table->string('first_name', 32);
            $table->string('last_name', 32);
            $table->string('faculty_id', 32)->unique();
            $table->string('email_address', 32);
            $table->string('department', 4);
            $table->string('phone_number', 14);
            $table->string('barcode_number', 32);
            $table->engine = 'InnoDB';
        });
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('faculty');
	}

}
