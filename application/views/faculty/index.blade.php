@layout('master')

@section('content')
    <h1 class="page-header">
        View Faculty
    </h1>

    @if (Session::get('success_message'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <p>
            {{ Session::get('success_message') }}
        </p>
    </div>
    @endif

    @if (Session::get('error_message'))
    <div class="alert alert-error alert-block">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <p>
            {{ Session::get('error_message') }}
        </p>
    </div>
    @endif

    <table id="faculty" class="datatable table table-striped table-bordered table-hover" data-datatable-not-sortable="6">
        <thead>
            <tr>
                <th>Faculty ID</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Department</th>
                <th>Phone</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($faculty as $f)
            <tr class=" ">
                <td>{{ strtoupper(e($f->faculty_id)) }}</td>
                <td>{{ e($f->first_name) }}</td>
                <td>{{ e($f->last_name) }}</td>
                <td>{{ e($f->email_address) }}</td>
                <td>{{ e($f->department) }}</td>
                <td>{{ e($f->phone_number) }}</td>
                <td>
                    <div class="btn-group">
                        <a class="btn btn-small" href="{{ URL::to_action('faculty/show', array($f->id)) }}">
                            Show
                        </a>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection

@section('page_specific_js')
    <script>
    </script>
@endsection
