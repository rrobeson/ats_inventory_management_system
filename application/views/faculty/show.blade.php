@layout('master')

@section('content')
    <h1 class="page-header">
        Faculty: {{ e($faculty->name()) }}
    </h1>

    <p>
        <strong>ID:</strong>
        {{ strtoupper(e($faculty->faculty_id)) }}
    </p>

    <p>
        <strong>Email:</strong>
        {{ e($faculty->email_address) }}
    </p>
    <p>
        <strong>Phone Number:</strong>
        {{ e($faculty->phone_number) }}
    </p>
    <p>
        <strong>Department</strong>
        {{ e($faculty->department) }}
    </p>


    @if($checkout_information)
        <h2>Checkouts</h2>
        @foreach($checkout_information as $c)
        <div class="row">
            <dl class="well span4">
                <dt>
                    <strong>Item:</strong>
                </dt>
                <dd>
                <a href="{{ URL::to_action('items/show', array('id' => $c->item_checked_out()->first()->id)) }}" target="_blank">
                        {{ e($c->item_checked_out()->first()->name) }}
                        ({{ e($c->item_checked_out()->first()->inventory_number) }})
                    </a>
                </dd>
                <dt>
                    <strong>Checked Out On:</strong>
                </dt>
                <dd>
                    {{ e(date('l F j, Y', strtotime($c->checked_out_date))) }}
                </dd>
                <dt>
                    <strong>Due:</strong>
                </dt>
                <dd>
                    {{ e(date('l F j, Y', strtotime($c->renewal_due_date ? $c->renewal_due_date : $c->due_date))) }}
                </dd>
                <dt>
                    <strong>Checked Out By:</strong>
                </dt>
                <dd>
                    {{ e($c->checked_out_by()->first()->first_name) }}
                    {{ e($c->checked_out_by()->first()->last_name) }}
                    <br />
                    Email: {{ e($c->checked_out_by()->first()->email) }}
                </dd>
            </dl>
        </div>
        @endforeach
    @endif

    @if($holds)
        <h2>Holds</h2>
        @foreach($holds as $h)
        <div class="row">
            <dl class="well span4">
                <dt>
                    <strong>Item:</strong>
                </dt>
                <dd>
                <a href="{{ URL::to_action('items/show', array('id' => $h->item()->first()->id)) }}" target="_blank">
                    {{ e($h->item()->first()->name) }}
                    ({{ e($h->item()->first()->inventory_number) }})
                </a>
                </dd>
                <dt>
                    <strong>Dates:</strong>
                </dt>
                <dd>
                    {{ e(date('l F j, Y', strtotime($h->start_date))) }}
                    -
                    {{ e(date('l F j, Y', strtotime($h->end_date))) }}
                </dd>
                <dt>
                    <strong>Approved By:</strong>
                </dt>
                <dd>
                    {{ e($h->approved_or_denied_by()->first()->first_name) }}
                    {{ e($h->approved_or_denied_by()->first()->last_name) }}
                    -
                    {{ e($h->approved_or_denied_by()->first()->email) }}
                </dd>
            </dl>
        </div>
        @endforeach
    @endif

@endsection

@section('page_specific_js')
@endsection
