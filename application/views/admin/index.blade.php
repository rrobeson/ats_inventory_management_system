@layout('master')

@section('content')

@if(Helperss::is_admin())
<div class="row">
    <div class="span4" style="text-align:center">
        <h2>Past Due</h2>
        <h2>
            <a href="{{ URL::to_action('reports/past_due_items') }}">{{ $past_due }}</a>
        </h2>
    </div>
    <div class="span4" style="text-align:center">
        <h2>Due Today</h2>
        <h2>
            <a href="{{ URL::to_action('reports/items_due_today') }}">{{ $due_today }}</a>
        </h2>
    </div>
    <div class="span4" style="text-align:center">
        <h2>Pending Holds</h2>
        <h2>
            <a href="{{ URL::to_action('holds/pending') }}">{{ $pending_holds }}</a>
        </h2>
    </div>
</div>
@endif

@if($user_level == 'Super Admin')
@endif

@if($user_level == 'Admin')
@endif

@if($user_level == 'Read Only Admin')
@endif

@endsection
