<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        {{ Asset::container('bootstrapper')->styles(); }}
        <link rel="stylesheet" type="text/css" href="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/css/jquery.dataTables.css">
        <link rel="stylesheet" type="text/css" href="{{ URL::to_asset('css/nv.d3.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::to_asset('css/datepicker.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::to_asset('css/table_tools.css') }}">

        <script src="{{ URL::to_asset('js/jquery.min.js') }}"></script>
        <script src="{{ URL::base() }}/bundles/bootstrapper/js/bootstrap.min.js"></script>
        <style>
            body {
                padding-top: 60px;
                padding-bottom: 40px;
            }

            /* Fix navbar on mobile */
            @media (min-width: 768px) and (max-width: 979px) {
                body {
                    padding-top: 0;
                }
            }

            @media (max-width: 767px) {
                body {
                    padding-top: 0;
                }
            }

            @media (max-width: 480px) {
                body {
                    padding-top: 0;
                }
            }
            /* End Fix navbar on mobile */
        </style>
        <script>
            // This path is used by DataTables to enable printing and downloading reports.
            window.sSwfPath = "{{ URL::to_asset('js/copy_csv_xls_pdf.swf') }}";
        </script>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="{{ URL::to_action('home/index') }}">ATS Inventory Management System</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav">
                            @if (Helperss::is_admin())
                            <li><a href="{{ URL::to_action('home/index') }}">Home</a></li>
                                <li><a href="{{ URL::to_action('items/checkout') }}">Checkout</a></li>
                                <li><a href="{{ URL::to_action('items/checkin') }}">Checkin</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="{{ URL::to_action('faculty/index') }}">Faculty</a></li>
                                    <li><a href="{{ URL::to_action('items/index') }}">Items</a></li>
                                    @if (Helperss::is_super_admin())
                                    <li><a href="{{ URL::to_action('item_categories/index') }}">Item Categories</a></li>
                                    @endif
                                    <li><a href="{{ URL::to_action('holds/pending') }}">Pending Holds</a></li>
                                    @if (Helperss::is_super_admin())
                                    <li><a href="{{ URL::to_action('users/index') }}">Users</a></li>
                                    @endif
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Reports <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li class="nav-header">Tabular</li>
                                    <li><a href="{{ URL::to_action('reports/available_items') }}">Available</a></li>
                                    <li><a href="{{ URL::to_action('reports/checkout_history') }}">Checkout History</a></li>
                                    <li><a href="{{ URL::to_action('reports/checked_out_items') }}">Currently Checked Out</a></li>
                                    <li><a href="{{ URL::to_action('reports/current_holds') }}">Current Holds</a></li>
                                    <li><a href="{{ URL::to_action('reports/damaged_lost_or_stolen') }}">Damaged, Lost, or Stolen Items</a></li>
                                    <li><a href="{{ URL::to_action('reports/items_checked_out_today') }}">Items Checked Out Today</a></li>
                                    <li><a href="{{ URL::to_action('reports/items_checked_out_by_faculty') }}">Items Checked Out By Faculty</a></li>
                                    <li><a href="{{ URL::to_action('reports/items_due_this_week') }}">Items Due This Week</a></li>
                                    <li><a href="{{ URL::to_action('reports/items_due_today') }}">Items Due Today</a></li>
                                    <li><a href="{{ URL::to_action('reports/past_due_items') }}">Past Due Items</a></li>
                                    <li><a href="{{ URL::to_action('reports/surplus') }}">Surplus Items</a></li>
                                    <li class="divider"></li>
                                    <li class="nav-header">Statistical</li>
                                    <li><a href="{{ URL::to_action('reports/items_checked_out_per_day') }}">Items Checked Out Per Day</a></li>
                                    <li><a href="{{ URL::to_action('reports/number_of_items') }}">Number of Items</a></li>
                                </ul>
                            </li>
                            @endif
                        </ul>
                        @if (Sentry::check())
                            <p class="navbar-text pull-right">
                                Hello, {{ Sentry::user()->get('first_name') }}
                                {{ Sentry::user()->get('last_name') }}.
                                <a href="{{ URL::to_action('admin/logout') }}">
                                    Logout?
                                </a>
                            </p>
                        @endif
                    </div><!--/.nav-collapse -->
                </div>
            </div>
        </div>

        <div class="container">
            @yield('content')

            <hr>

            <footer>
                <p>&copy; TechWorks 2013</p>
            </footer>
        </div> <!-- /container -->
        <script type="text/javascript" charset="utf8" src="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>
        <script src="{{ URL::to_asset('js/nvd3/lib/d3.v2.min.js') }}"></script>
        <script src="{{ URL::to_asset('js/nvd3/nv.d3.min.js') }}"></script>
        <script src="{{ URL::to_asset('js/bootstrap-datepicker.js') }}"></script>
        <script src="{{ URL::to_asset('js/TableTools.min.js') }}"></script>
        <script src="{{ URL::to_asset('js/site.js') }}"></script>
        @section('page_specific_js')
        @yield_section
    </body>
</html>
