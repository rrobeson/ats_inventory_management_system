<!DOCTYPE html>
<html>
    <head>
        <title>Requesting a Hold</title>
        <meta charset="utf-8" />
    </head>
    <body>

        <h1>Requesting a hold for: {{ $item->name }}</h1>
        <p>
            Please fill out the form below. All fields are required. We will use this information to contact you
            regarding the status of your hold as soon as we have reviewed your request.
        </p>

        <p>
            {{ $errors->has('save_error') ? $errors->get('save_error')[0] : '' }}
        </p>
        
        {{ Form::open('holds/request_hold', 'POST') }} 
            <fieldset>
                <label for="faculty_id">Faculty ID(E00000000)</label>
                <input name="faculty_id" type="text" value="{{ e(Input::old('faculty_id')) }}" />
                {{ $errors->has('faculty_id') ? implode($errors->get('faculty_id', '<span>:message</span>')) : '' }}
                <br />

                <label for="first_name">First Name</label>
                <input name="first_name" type="text" value="{{ e(Input::old('first_name')) }}" />
                {{ $errors->has('first_name') ? implode($errors->get('first_name', '<span>:message</span>')) : '' }}
                <br />

                <label for="last_name">Last Name</label>
                <input name="last_name" type="text" value="{{ e(Input::old('last_name')) }}" />
                {{ $errors->has('last_name') ? implode($errors->get('last_name', '<span>:message</span>')) : '' }}
                <br />

                <label for="email_address">Email Address</label>
                <input name="email_address" type="text" value="{{ e(Input::old('email_address')) }}" />
                {{ $errors->has('email_address') ? implode($errors->get('email_address', '<span>:message</span>')) : '' }}
                <br />

                <label for="department">Department</label>
                <input name="department" type="text" value="{{ e(Input::old('department')) }}" />
                {{ $errors->has('department') ? implode($errors->get('department', '<span>:message</span>')) : '' }}
                <br />

                <label for="phone_number">Phone Number(4235555555)</label>
                <input name="phone_number" type="text" value="{{ e(Input::old('phone_number')) }}" />
                {{ $errors->has('phone_number') ? implode($errors->get('phone_number', '<span>:message</span>')) : '' }}
                <br />

                <label for="start_date">Start Date(mm/dd/yyyy)</label>
                <input name="start_date" type="text" value="{{ e(Input::old('start_date')) }}" />
                {{ $errors->has('start_date') ? implode($errors->get('start_date', '<span>:message</span>')) : '' }}
                <br />

                <label for="end_date">End Date(mm/dd/yyyy)</label>
                <input name="end_date" type="text" value="{{ e(Input::old('end_date')) }}" />
                {{ $errors->has('end_date') ? implode($errors->get('end_date', '<span>:message</span>')) : '' }}
                <br />

                <input name="item_id" type="hidden" value="{{ $item->id }}" />

                <input type="submit" value="Request Hold" />
                <a href="{{ URL::to_action('holds/index') }}">Cancel</a>
            </fieldset>
        {{ Form::close() }}
    </body>
</html>
