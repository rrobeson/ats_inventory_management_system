@layout('master')

@section('content')
    <h1 class="page-header">
        Manage Pending Hold Requests
    </h1>

    @if (Session::get('success_message'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <p>
            {{ Session::get('success_message') }}
        </p>
    </div>
    @endif

    @if (Session::get('error_message'))
    <div class="alert alert-error alert-block">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <p>
            {{ Session::get('error_message') }}
        </p>
    </div>
    @endif

    <table id="categories" class="datatable table table-striped table-bordered table-hover" data-datatable-not-sortable="8">
        <thead>
            <tr>
                <th>Item Name</th>
                <th>Item Inventory Number</th>
                <th>Requester Name</th>
                <th>Requester Phone</th>
                <th>Requester Email</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Type</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($holds as $h)
            <tr class=" ">
                <td><a href="{{ URL::to_action('items/show', array($h->item()->first()->id)) }}" target="_blank">{{ e($h->item()->first()->name) }}</a></td>
                <td>{{ e($h->item()->first()->inventory_number) }}</td>
                <td>
                    <a href="{{ URL::to_action('faculty/show', array('id' => $h->held_for()->first()->id)) }}" target="_blank">
                    {{ e($h->held_for()->first()->name()) }}
                    </a>
                </td>
                <td>{{ e($h->held_for()->first()->phone_number) }}</td>
                <td>{{ e($h->held_for()->first()->email_address) }}</td>
                <td>{{ date('l F j, Y', strtotime(e($h->start_date))) }}</td>
                <td>{{ date('l F j, Y', strtotime(e($h->end_date))) }}</td>
                <td>{{ $h->type() }}</td>
                <td>
                    <div class="btn-group">
                    {{ Form::open('holds/approve', 'POST', array('class' => '', 'style' => 'display:inline')) }}
                        {{ Form::hidden('id', $h->id) }}
                        {{ Form::submit('Approve', array('class' => 'btn btn-success btn-small')) }}
                    {{ Form::close() }}
                    {{ Form::open('holds/deny', 'POST', array('class' => '', 'style' => 'display:inline')) }}
                        {{ Form::hidden('id', $h->id) }}
                        {{ Form::submit('Deny', array('class' => 'btn btn-danger btn-small')) }}
                    {{ Form::close() }}
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection

@section('page_specific_js')
    <script>
    </script>
@endsection
