<!DOCTYPE html>
<html>
    <head>
        <title>Request Sent</title>
        <meta charset="utf-8" />
    </head>
    <body>
        <header>
            <h1>Your request has been sent</h1>
        </header>
        <p>
            You will be contacted once your request has been approved. Have a nice day.
            <br />
            <a href="{{ URL::to_action('holds/index') }}">Request another item?</a>
        </p>
    </body>
</html>
