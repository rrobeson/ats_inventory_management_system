<!DOCTYPE html>
<html>
    <head>
        <title>Viewing Items</title>
        <meta charset="utf-8" />
    </head>
    <body>
        <p>Select an item below to request a hold on it. The keyboard shortcut "Control-F" can be used to search for an item by its name.</p>
        <table>
            <tbody>
                <tr>
                    <th>Inventory Number</th>
                    <th>Item name</th>
                    <th>Category</th>
                    <th>Allowed Checkout Length(days)</th>
                    <th>Request</th>
                </tr>
                @foreach($items as $i)
                <tr>
                    <td>{{ $i->inventory_number }}</td>
                    <td>{{ $i->name }}</td>
                    <td>{{ $i->category()->first()->name }}</td>
                    <td>{{ $i->allowed_checkout_length }}</td>
                    <td>
                        <a href="{{ URL::to_action('holds/request_hold', array($i->id)) }}">Request Hold</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </body>
</html>
