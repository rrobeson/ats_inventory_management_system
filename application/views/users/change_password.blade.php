@layout('master')

@section('content')
    <div class="page-header">
        <h1>Changing Password For {{ $user->first_name . ' ' . $user ->last_name }} ({{ $user->email }})</h1>
    </div>
    {{ Form::open('users/change_password', 'POST', array('class' => '')) }}
        <fieldset>
            @if (Session::get('errors'))
            <div class="alert alert-error alert-block">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
            </div>
            @endif    

            <div class="control-group {{ $errors->has('password') ? 'error': '' }}">
                {{ Form::label('password', 'New Password') }}
                <div class="controls">
                    {{ Form::password('password', array('required' => 'required')) }}
                </div>
            <div>
            <div class="control-group {{ $errors->has('password_confirmation') ? 'error': '' }}">
                {{ Form::label('password_confirmation', 'Password Confirmation') }}
                <div class="controls">
                    {{ Form::password('password_confirmation', array('required' => 'required')) }}
                </div>
            <div>
            {{ Form::hidden('id', $user->id) }}
            <br />
            <div class="control-group">
                <div class="controls">
                    {{ Form::submit('Change Password', array('class' => 'btn btn-primary')) }}
                    <a href="{{ URL::to_action('users/edit', array('id' => $user->id)) }}" class="btn">Cancel</a>
                </div>
            </div>
        </fieldset>
    {{ Form::close() }}
@endsection

@section('page_specific_js')
@endsection
