@layout('master')

@section('content')
    <div class="page-header">
        <h1>Editing {{ $user->first_name . ' ' . $user ->last_name }} ({{ $user->email }})</h1>
    </div>
    {{ Form::open('users/edit', 'POST', array('class' => '')) }}
        <fieldset>
            @if (Session::get('errors'))
            <div class="alert alert-error alert-block">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
            </div>
            @endif    

            <div class="control-group {{ $errors->has('first_name') ? 'error': '' }}">
                {{ Form::label('first_name', 'First Name') }}
                <div class="controls">
                    {{ Form::text('first_name', Input::had("first_name") ? Input::old("first_name") : $user->first_name, array('required' => 'required')) }}
                </div>
            <div>
            <div class="control-group {{ $errors->has('last_name') ? 'error': '' }}">
                {{ Form::label('last_name', 'Last Name') }}
                <div class="controls">
                    {{ Form::text('last_name', Input::had("last_name") ? Input::old("last_name") : $user->last_name, array('required' => 'required')) }}
                </div>
            <div>
            <div class="control-group {{ $errors->has('access_level') ? 'error' : '' }}">
                {{ Form::label('access_level', 'Access Level', array('class' => 'control-label')) }}
                <div class="controls">
                    <select name="access_level" required="required">
                        @foreach (User::valid_access_levels() as $label => $identifier)
                        <option value="{{ e($identifier) }}"
                        @if (Input::old('access_level') == $identifier)
                            selected="selected"
                        @elseif (!Input::has('access_level') && strtoupper($user->access_level()) == strtoupper($label))
                            selected="selected"
                        @endif
                        >{{ e($label) }}</option>
                        @endforeach
                    </select>
                </div>
            {{ Form::hidden('id', $user->id) }}
            <br />
            <div class="control-group">
                <div class="controls">
                    {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
                    <a href="{{ URL::to_action('users/index') }}" class="btn">Cancel</a>
                </div>
            </div>
            <a href="{{ URL::to_action('users/change_password', array('id' => $user->id)) }}">
                Change Password
            </a>
        </fieldset>
    {{ Form::close() }}
@endsection

@section('page_specific_js')
@endsection
