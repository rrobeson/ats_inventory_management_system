@layout('master')

@section('content')
    <h1 class="page-header">
        Manage Users
        <p>
            <a href="{{ URL::to_action('users/new') }}" class="btn btn-primary">
                New User
            </a>
        </p>
    </h1>

    @if (Session::get('errors'))
    <div class="alert alert-error alert-block">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
        </ul>
    </div>
    @endif    

    @if (Session::get('success_message'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <p>
            {{ Session::get('success_message') }}
        </p>
    </div>
    @endif

    @if (Session::get('error_message'))
    <div class="alert alert-error alert-block">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <p>
            {{ Session::get('error_message') }}
        </p>
    </div>
    @endif

    <table id="users" class="datatable table table-striped table-bordered table-hover" data-datatable-not-sortable="5">
        <thead>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>User level</th>
                <th>User ID</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($users as $u)
            <tr class="
            @if($u->access_level() == 'Read Only Admin')
                success
            @elseif($u->access_level() == 'Admin')
                warning
            @elseif($u->access_level() == 'Super Admin')
                error
            @endif
            ">
                <td>{{ $u->first_name }}</td>
                <td>{{ $u->last_name }}</td>
                <td>{{ $u->email }}</td>
                <td>{{ $u->access_level() }}</td>
                <td>{{ $u->userid }}</td>
                <td>
                    @if ($u->activated)
                        {{ Form::open('users/deactivate', 'POST', array('class' => '')) }}
                    @else
                        {{ Form::open('users/activate', 'POST', array('class' => '')) }}
                    @endif
                    <div class="btn-group">
                        <a class="btn btn-small" href="{{ URL::to_action('users/show', array($u->id)) }}">
                            Show
                        </a>
                        <a class="btn btn-small" href="{{ URL::to_action('users/edit', array($u->id)) }}">
                            Edit
                        </a>
                        {{ Form::hidden('id', $u->id) }}
                        @if ($u->activated)
                            {{ Form::submit('Deactivate', array('class' => 'btn btn-danger btn-small')) }}
                        @else
                            {{ Form::submit('Activate', array('class' => 'btn btn-success btn-small')) }}
                        @endif
                                
                    </div>
                    {{ Form::close() }}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection

@section('page_specific_js')
@endsection
