@layout('master')

@section('content')
    <div class="page-header">
        <h1>New User</h1>
    </div>
    {{ Form::open('users/new', 'POST', array('class' => '')) }}
        <fieldset>
            @if (Session::get('errors'))
            <div class="alert alert-error alert-block">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
            </div>
            @endif    

            <div class="control-group {{ $errors->has('email') ? 'error' : '' }}">
                {{ Form::label('email', 'Email', array('class'=> 'control-label')) }}
                <div class="controls">
                    {{ Form::text('email', Input::old("email"), array('required' => 'required', 'placeholder' => 'johnd@mail.etsu.edu')) }}
                </div>
            </div>
            <div class="control-group {{ $errors->has('first_name') ? 'error' : '' }}">
                {{ Form::label('first_name', 'First Name', array('class'=> 'control-label')) }}
                <div class="controls">
                    {{ Form::text('first_name', Input::old("first_name"), array('required' => 'required', 'placeholder' => 'John')) }}
                </div>
            </div>
            <div class="control-group {{ $errors->has('last_name') ? 'error' : '' }}">
                {{ Form::label('last_name', 'Last Name', array('class'=> 'control-label')) }}
                <div class="controls">
                    {{ Form::text('last_name', Input::old("last_name"), array('required' => 'required', 'placeholder' => 'Doe')) }}
                </div>
            </div>
            <div class="control-group {{ $errors->has('userid') ? 'error' : '' }}">
                {{ Form::label('userid', 'User ID', array('class'=> 'control-label')) }}
                <div class="controls">
                    {{ Form::text('userid', Input::old("userid"), array('required' => 'required', 'placeholder' => 'E00000000', 'pattern' => 'E.{8}')) }}
                </div>
            </div>
            <div class="control-group {{ $errors->has('access_level') ? 'error' : '' }}">
                {{ Form::label('access_level', 'Access Level', array('class' => 'control-label')) }}
                <div class="controls">
                    <select name="access_level" required="required">
                    <option></option>
                    @foreach (User::valid_access_levels() as $label => $identifier)
                    <option value="{{ e($identifier) }}"
                    @if (Input::old('access_level') == $identifier)
                        selected="selected"
                    @endif
                    >
                        {{ e($label) }}
                    </option>
                    @endforeach
                    </select>
                </div>
            </div>
            <div class="control-group {{ $errors->has('password') ? 'error' : '' }}">
                {{ Form::label('password', 'Password', array('class'=> 'control-label')) }}
                <div class="controls">
                    {{ Form::password('password', array('required' => 'required')) }}
                </div>
            </div>
            <div class="control-group {{ $errors->has('password_confirmation') ? 'error' : '' }}">
                {{ Form::label('password_confirmation', 'Password Confirmation', array('class'=> 'control-label')) }}
                <div class="controls">
                    {{ Form::password_confirmation('password_confirmation', array('required' => 'required')) }}
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    {{ Form::submit('Create', array('class' => 'btn btn-primary')) }}
                    <a href="{{ URL::to_action('users/index') }}" class="btn">Cancel</a>
                </div>
            </div>
        </fieldset>
    {{ Form::close() }}
@endsection

@section('page_specific_js')
@endsection
