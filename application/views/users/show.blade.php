@layout('master')

@section('content')
    <h1 class="page-header">
        User: {{ e($user->first_name) }} {{ e($user->last_name) }} ({{ e($user->email) }})
    </h1>

    <p>
        <strong>First Name:</strong>
        {{ e($user->first_name) }}
    </p>

    <p>
        <strong>Last Name:</strong>
        {{ e($user->last_name) }}
    </p>

    <p>
        <strong>Email:</strong>
        {{ e($user->email) }}
    </p>

    <p>
        <strong>User ID:</strong>
        {{ e($user->userid) }}
    </p>

    <p>
        <strong>Access Level:</strong>
        {{ e($user->access_level()) }}
    </p>

    <p>
        <strong>Last Login:</strong>
        {{ e($user->last_login) }}
    </p>

    <p>
        This user is
        @if ($user->activated)
            active
        @else
            inactive
        @endif
    </p>

    <div class="btn-group">
        <a href="{{ URL::to_action('users/edit', array('id' => $user->id)) }}" class="btn">Edit</a>
        <a href="{{ URL::to_action('users/index') }}" class="btn">Back</a>
    </div>
@endsection

@section('page_specific_js')
@endsection
