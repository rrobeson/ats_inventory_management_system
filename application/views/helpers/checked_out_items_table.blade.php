    <table id="items" class="datatable table table-striped table-bordered table-hover" data-datatable-not-sortable="8" data-datatable-not-visible="5,6,7">
        <thead>
            <tr>
                <th>ETSU Number</th>
                <th>Name</th>
                <th>Serial Number</th>
                <th>Due</th>
                <th>Checked Out To</th>
                <th>Category</th>
                <th>Keywords</th>
                <th>Description</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($items as $i)
            <tr class=" ">
                <td>{{ strtoupper(e($i->inventory_number)) }}</td>
                <td>{{ e($i->name) }}</td>
                <td>{{ e($i->serial_number) }}</td>
                <td>{{ e(date('l F j, Y', strtotime($i->current_check_out()->due()))) }}</td>
                <td>
                    <a href="{{ URL::to_action('faculty/show', array('id' => $i->current_check_out()->checked_out_to()->first()->id)) }}" target="_blank">
                        {{ $i->current_check_out()->checked_out_to()->first()->name() }}
                    </a>
                </td>
                <td>
                    {{ e($i->category()->first()->name) }}
                </td>
                <td>
                    {{ e($i->keywords) }}
                </td>
                <td>
                    {{ e($i->description) }}
                </td>
                <td>
                    <div class="btn-group">
                        <a class="btn btn-small" href="{{ URL::to_action('items/show', array($i->id)) }}" target="_blank">
                            Show
                        </a>
                        <a class="btn btn-small" href="{{ URL::to_action('items/edit', array($i->id)) }}" target="_blank">
                            Edit
                        </a>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
