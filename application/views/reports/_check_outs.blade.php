@layout('master')

@section('content')
    <h1 class="page-header">
        {{ $title }}
    </h1>

    @if (Session::get('success_message'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <p>
            {{ Session::get('success_message') }}
        </p>
    </div>
    @endif

    @if (Session::get('error_message'))
    <div class="alert alert-error alert-block">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <p>
            {{ Session::get('error_message') }}
        </p>
    </div>
    @endif
    
    <table id="items" class="datatable table table-striped table-bordered table-hover" data-datatable-not-sortable="11" data-datatable-not-visible="8,9,10">
        <thead>
            <tr>
                <th>ETSU Number</th>
                <th>Name</th>
                <th>Serial Number</th>
                <th>Checked Out On</th>
                <th>Due</th>
                <th>Checked Out By</th>
                <th>Checked Out To</th>
                <th>Checked In On</th>
                <th>Category</th>
                <th>Keywords</th>
                <th>Description</th>
                <th>Item Actions</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($transactions as $t)
            <tr class=" ">
                <td>{{ strtoupper(e($t->item_checked_out()->first()->inventory_number)) }}</td>
                <td>{{ e($t->item_checked_out()->first()->name) }}</td>
                <td>{{ e($t->item_checked_out()->first()->serial_number) }}</td>
                <td>{{ e(date('l F j, Y', strtotime($t->checked_out_date))) }}</td>
                <td>{{ e(date('l F j, Y', strtotime($t->due()))) }}</td>
                <td>
                    <a href="{{ URL::to_action('users/show', array('id' => $t->checked_out_by()->first()->id)) }}" target="_blank">
                        {{ $t->checked_out_by()->first()->name() }}
                    </a>
                </td>
                <td>
                    <a href="{{ URL::to_action('faculty/show', array('id' => $t->checked_out_to()->first()->id)) }}" target="_blank">
                        {{ $t->checked_out_to()->first()->name() }}
                    </a>
                </td>
                <td>
                @if ($t->checked_in_date)
                    {{ e(date('l F j, Y', strtotime($t->checked_in_date))) }}
                @else
                    Has not been checked in.
                @endif
                </td>
                <td>
                    {{ e($t->item_checked_out()->first()->category()->first()->name) }}
                </td>
                <td>
                    {{ e($t->item_checked_out()->first()->keywords) }}
                </td>
                <td>
                    {{ e($t->item_checked_out()->first()->description) }}
                </td>
                <td>
                    <div class="btn-group">
                        <a class="btn btn-small" href="{{ URL::to_action('items/show', array($t->item_checked_out()->first()->id)) }}" target="_blank">
                            Show
                        </a>
                        <a class="btn btn-small" href="{{ URL::to_action('items/edit', array($t->item_checked_out()->first()->id)) }}" target="_blank">
                            Edit
                        </a>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection

@section('page_specific_js')
@endsection
