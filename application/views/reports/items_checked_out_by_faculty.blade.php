@layout('master')

@section('content')
    <h1 class="page-header">
        Items Checked Out by Faculty
    </h1>

    @if (Session::get('success_message'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <p>
            {{ Session::get('success_message') }}
        </p>
    </div>
    @endif

    @if (Session::get('error_message'))
    <div class="alert alert-error alert-block">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <p>
            {{ Session::get('error_message') }}
        </p>
    </div>
    @endif

    {{ Form::open('reports/items_checked_out_by_faculty', 'POST', array('class' => 'form')) }}
    <fieldset>
        @if (Session::get('errors'))
        <div class="alert alert-error alert-block">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
            </ul>
        </div>
        @endif    
        <div class="control-group {{ $errors->has('faculty_id') ? 'error' : '' }}">
            {{ Form::label('faculty_id', 'Faculty ID', array('class'=> 'control-label')) }}
            <div class="controls">
                <input name="faculty_id" type="text" value="{{ Input::old("faculty_id") }}" placeholder="E00000000" data-provide="typeahead" data-minLength="4" data-source="[{{ implode(',', $faculty_ids) }}]" autocomplete="off" />
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                {{ Form::submit('View Report', array('class' => 'btn btn-primary')) }}
            </div>
        </div>
    </fieldset>
    {{ Form::close() }}

@endsection

@section('page_specific_js')
    <script>
    </script>
@endsection
