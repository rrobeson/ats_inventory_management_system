@layout('master')

@section('content')
    <h1 class="page-header">
        Available Items
    </h1>

    @if (Session::get('success_message'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <p>
            {{ Session::get('success_message') }}
        </p>
    </div>
    @endif

    @if (Session::get('error_message'))
    <div class="alert alert-error alert-block">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <p>
            {{ Session::get('error_message') }}
        </p>
    </div>
    @endif

    <table id="items" class="datatable table table-striped table-bordered table-hover" data-datatable-not-sortable="7">
        <thead>
            <tr>
                <th>ETSU Number</th>
                <th>Name</th>
                <th>Serial Number</th>
                <th style="text-align:right">Value</th>
                <th>Category</th>
                <th style="text-align:right">Allowed Checkout Length(days)</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($items as $i)
            <tr class=" ">
                <td>{{ strtoupper(e($i->inventory_number)) }}</td>
                <td>{{ e($i->name) }}</td>
                <td>{{ e($i->serial_number) }}</td>
                <td style="text-align:right">{{ money_format('$%i', e($i->monetary_value)) }}</td>
                <td>{{ e($i->category()->first()->name) }}</td>
                <td style="text-align:right">{{ e($i->allowed_checkout_length) }}</td>
                <td>{{ ucfirst(e($i->status)) }}</td>
                <td>
                    <div class="btn-group">
                        <a class="btn btn-small" target="_blank" href="{{ URL::to_action('items/show', array($i->id)) }}">
                            Show
                        </a>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection

@section('page_specific_js')
@endsection
