@layout('master')

@section('content')
    <h1 class="page-header">
        Checkout History
    </h1>

    @if (Session::get('success_message'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <p>
            {{ Session::get('success_message') }}
        </p>
    </div>
    @endif

    @if (Session::get('error_message'))
    <div class="alert alert-error alert-block">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <p>
            {{ Session::get('error_message') }}
        </p>
    </div>
    @endif

    <div class="row">
        <div id="alert" class="span12 alert alert-error" style="display:none">
            <strong>Uh oh!</strong>
        </div>
    </div>

    {{ Form::open('reports/checkout_history', 'POST', array('class' => 'form')) }}
    <fieldset>
        @if (Session::get('errors'))
        <div class="alert alert-error alert-block">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
            </ul>
        </div>
        @endif    
        <div class="control-group {{ $errors->has('start_date') ? 'error' : '' }}">
            {{ Form::label('start_date', 'Start Date', array('class'=> 'control-label')) }}
            <div class="controls">
                <input id="start_date" name="start_date" class="datepicker" type="text" value="{{ $start_date }}" required data-provide="datepicker" autocomplete="off" data-date-format="yyyy/mm/dd" />
            </div>
        </div>
        <div class="control-group {{ $errors->has('end_date') ? 'error' : '' }}">
            {{ Form::label('end_date', 'End Date', array('class'=> 'control-label')) }}
            <div class="controls">
                <input id="end_date" name="end_date" class="datepicker" type="text" value="{{ $end_date }}" required data-provide="datepicker" autocomplete="off" data-date-format="yyyy/mm/dd" />
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <a class="btn" href="{{ URL::to_action('reports/all_checkouts') }}">Show <em>Everything</em></a>
                {{ Form::submit('View', array('class' => 'btn btn-primary')) }}
            </div>
        </div>
    </fieldset>
    {{ Form::close() }}

@endsection

@section('page_specific_js')
    <script>
        $(function() {
            $(".datepicker").datepicker();
        });

        // These are used simply to ensure the dates are a valid range
        var startDate = new Date('{{ $start_date }}');
        var endDate = new Date('{{ $end_date }}');

        // Handle the change event for the start date
        $('#start_date')
            .datepicker()
            .on('changeDate', function(ev){
                if (ev.date.valueOf() > endDate.valueOf()){
                    $('#alert').show().find('strong').text('The start date must be before the end date.');
                } else {
                    $('#alert').hide();
                    startDate = new Date(ev.date);
                }
                $('#start_date').datepicker('hide');
            });

        // Handle the change event for the end date
        $('#end_date')
            .datepicker()
            .on('changeDate', function(ev){
                if (ev.date.valueOf() < startDate.valueOf()){
                    $('#alert').show().find('strong').text('The end date must be after the start date.');
                } else {
                    $('#alert').hide();
                    endDate = new Date(ev.date);
                }
                $('#end_date').datepicker('hide');
            });
    </script>
@endsection
