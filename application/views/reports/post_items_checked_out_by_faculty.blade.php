@layout('master')

@section('content')
    <h1 class="page-header">
        Items checked out by {{ e($items[0]['checked_out_to']->first_name) }} {{ e($items[0]['checked_out_to']->last_name) }}
    </h1>

    <div class="well">
        <h3>Contact</h3>
        <ul class="unstyled">
            <li>Phone: {{ e($items[0]['checked_out_to']->phone_number) }}</li>
            <li>Email: {{ e($items[0]['checked_out_to']->email_address) }}</li>
        </ul>
    </div>

    @if (Session::get('success_message'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <p>
            {{ Session::get('success_message') }}
        </p>
    </div>
    @endif

    @if (Session::get('error_message'))
    <div class="alert alert-error alert-block">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <p>
            {{ Session::get('error_message') }}
        </p>
    </div>
    @endif

    <table id="items" class="datatable table table-striped table-bordered table-hover" data-datatable-not-sortable="4">
        <thead>
            <tr>
                <th>ETSU Number</th>
                <th>Name</th>
                <th>Serial Number</th>
                <th>Due</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($items as $i)
            <tr class=" ">
                <td>{{ strtoupper(e($i['item']->inventory_number)) }}</td>
                <td>{{ e($i['item']->name) }}</td>
                <td>{{ e($i['item']->serial_number) }}</td>
                <td>{{ e($i['due_date']) }}</td>
                <td>
                    <div class="btn-group">
                        <a class="btn btn-small" href="{{ URL::to_action('items/show', array($i['item']->id)) }}" target="_blank">
                            Show
                        </a>
                        <a class="btn btn-small" href="{{ URL::to_action('items/edit', array($i['item']->id)) }}" target="_blank">
                            Edit
                        </a>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection

@section('page_specific_js')
@endsection
