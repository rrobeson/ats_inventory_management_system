@layout('master')

@section('content')
    <h1 class="page-header">
        Number of Items
    </h1>

    @if (Session::get('success_message'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <p>
            {{ Session::get('success_message') }}
        </p>
    </div>
    @endif

    @if (Session::get('error_message'))
    <div class="alert alert-error alert-block">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <p>
            {{ Session::get('error_message') }}
        </p>
    </div>
    @endif

    
    <div class="navbar">
        <div class="navbar-inner">
            <ul class="nav data-choice">
                <li class="active"><a href="#" data-value="ibc">By Category</a></li>
                <li><a href="#" data-value="ibs">By Status</a></li>
            </ul>
        </div>
    </div>

    <div id="chart">
        <svg></svg>
    </div>

@endsection

@section('page_specific_js')
    <script>
        // Store the values from the server to use
        // when switching between navigation items.
        values = {
            "ibc": {{ $ibc_values }},
            "ibs": {{ $ibs_values }}
        };

        $(".data-choice li a").click(function() {
            var selection = $(this).attr('data-value');

            $(".data-choice li").removeClass('active');
            $(this).parent().addClass('active');

            redraw(values[selection]);
        });

        $(function() {
            // Initial load.
            draw(values["ibc"]);
        });

        // Draw the graph with the given values
        draw = function(v) {
            var width = 800;
            var height = 500;

            nv.addGraph(function() {
                var chart = nv.models.pieChart()
                    .x(function(d) { return d.key })
                    .y(function(d) { return d.y })
                    .values(function(d) { return d })
                    .color(d3.scale.category10().range())
                    .width(width)
                    .height(height)
                    .showLabels(true)
                    .margin({top: 30, right: 20, bottom: 20, left: 120});

                d3.select('#chart svg')
                    .datum([v])
                    .transition().duration(500)
                    .attr('width', width)
                    .attr('height', height)
                    .call(chart);

                nv.utils.windowResize(function() { d3.select('#chart svg').call(chart) });

                return chart;
            });
        };

        redraw = function(v) {
            $("#chart svg").empty();

            draw(v);
        };
    </script>
@endsection
