@layout('master')

@section('content')
    <h1 class="page-header">
        Current Holds
    </h1>

    @if (Session::get('success_message'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <p>
            {{ Session::get('success_message') }}
        </p>
    </div>
    @endif

    @if (Session::get('error_message'))
    <div class="alert alert-error alert-block">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <p>
            {{ Session::get('error_message') }}
        </p>
    </div>
    @endif

    <table id="holds" class="datatable table table-striped table-bordered table-hover" data-datatable-not-sortable="1,2,3,6">
        <thead>
            <tr>
                <th>Item</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Date Approved</th>
                <th>Held For</th>
                <th>Approved By</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($holds as $h)
            <tr class=" ">
                <td>
                    <a href="{{ URL::to_action('items/show', array('id' => $h->item()->first()->id))}}" target="_blank">
                        {{ e($h->item()->first()->name) }}
                        ({{e($h->item()->first()->inventory_number) }})
                    </a>
                </td>
                <td>{{ e(date('l F j, Y', strtotime($h->start_date))) }}</td>
                <td>{{ e(date('l F j, Y', strtotime($h->end_date))) }}</td>
                <td>{{ e(date('l F j, Y', strtotime($h->date_approved))) }}</td>
                <td>
                    <a href="{{ URL::to_action('faculty/show', array('id' => $h->held_for()->first()->id)) }}" target="blank">
                        {{ e($h->held_for()->first()->name() )}}
                    </a>
                </td>
                <td>{{ e($h->approved_or_denied_by()->first()->name()) }}</td>
                <td>
                    {{ Form::open('holds/complete', 'POST', array('class' => '', 'style' => 'display:inline')) }}
                        {{ Form::hidden('id', $h->id) }}
                        {{ Form::submit('Complete', array('class' => 'btn btn-small')) }}
                    {{ Form::close() }}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection

@section('page_specific_js')
@endsection
