@layout('master')

@section('content')
    <h1 class="page-header">
        Items Checked Out Per Day
    </h1>

    @if (Session::get('success_message'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <p>
            {{ Session::get('success_message') }}
        </p>
    </div>
    @endif

    @if (Session::get('error_message'))
    <div class="alert alert-error alert-block">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <p>
            {{ Session::get('error_message') }}
        </p>
    </div>
    @endif

    <div class="row">
        <div id="alert" class="span12 alert alert-error" style="display:none">
            <strong>Uh oh!</strong>
        </div>
    </div>

    <div class="row">
        <div class="span3 offset2">
            <label for="start_date">Start Date</label>
            <input id="start_date" name="start_date" type="text" value="{{ $start_date }}" class="datepicker" autocomplete="off" date-provide="datepicker" data-date-format="yyyy/mm/dd" />
        </div>

        <div class="span3 offset1">
            <label for="end_date">End Date</label>
            <input id="end_date" name="end_date" type="text" value="{{ $end_date }}" class="datepicker" autocomplete="off" date-provide="datepicker" data-date-format="yyyy/mm/dd" />
        </div>
    </div>

    <div id="chart" style="height:400px">
        <svg></svg>
    </div>

@endsection

@section('page_specific_js')
    <script>
        $(function() {
            $(".datepicker").datepicker();

            // Initial load.
            draw({{ $values }});
        });

        // These are used simply to ensure the dates are a valid range
        var startDate = new Date('{{ $start_date }}');
        var endDate = new Date('{{ $end_date }}');

        // Handle the change event for the start date
        $('#start_date')
            .datepicker()
            .on('changeDate', function(ev){
                if (ev.date.valueOf() > endDate.valueOf()){
                    $('#alert').show().find('strong').text('The start date must be before the end date.');
                } else {
                    $('#alert').hide();
                    startDate = new Date(ev.date);
                    // Redraw the graph with the new date range
                    redraw('items_checked_out_per_day', $('#start_date').val(), $('#end_date').val());
                }
                $('#start_date').datepicker('hide');
            });

        // Handle the change event for the end date
        $('#end_date')
            .datepicker()
            .on('changeDate', function(ev){
                if (ev.date.valueOf() < startDate.valueOf()){
                    $('#alert').show().find('strong').text('The end date must be after the start date.');
                } else {
                    $('#alert').hide();
                    endDate = new Date(ev.date);
                    // Redraw the graph with the new date range
                    redraw('items_checked_out_per_day', $('#start_date').val(), $('#end_date').val());
                }
                $('#end_date').datepicker('hide');
            });

        // Draw the graph with the given values
        draw = function(v) {
            values = [
                        {
                            values: v,
                            key: 'Items checked out'
                        }
                     ];

            for(var i=0; i<values[0].values.length; i++) {
                values[0].values[i].x = new Date(values[0].values[i].x);
            }

            nv.addGraph(function() {
                var chart = nv.models.discreteBarChart()
                    .forceY([0])
                    .margin({top: 30, right: 60, bottom: 100, left: 80});

                chart.xAxis
                    .axisLabel('Date')
                    .rotateLabels(-45)
                    .tickFormat(function(d) { return d3.time.format('%B %d, %Y')(new Date(d))});

                chart.yAxis
                    .tickFormat(d3.format('d'))
                    .axisLabel('Checkouts');

                d3.select('#chart svg')
                    .datum(values)
                    .transition().duration(500)
                    .call(chart);

                nv.utils.windowResize(function() { d3.select('#chart svg').call(chart) });

                return chart;
            });
        };

        // Redraw the graph with values from an AJAX request
        redraw = function(url, start_date, end_date) {
            $.post(url, { start_date: start_date, end_date: end_date }, function(data) {
                $("#chart svg").empty();

                draw(JSON.parse(data));
            });
        };
    </script>
@endsection
