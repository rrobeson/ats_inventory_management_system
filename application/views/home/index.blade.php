@layout('master')

@section('content')
    <div class="hero-unit">
        <h1>ATS Personnel</h1>
        <p>Sign in below to access the ATS Inventory Management System</p>
    </div>

    {{ Form::open('admin/login', 'POST', array('class' => '')) }}
        <fieldset>
            <legend>Sign in</legend>
            
            @if (Session::get('errors'))
            <p class="alert alert-error">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                Invalid Email or Password
            </p>
            @endif

            {{ Form::label('email','Email') }}
            {{ Form::text('email', Input::old('email'), array('class' => '', 'placeholder' => 'Email')) }}
            {{ Form::label('password','Password') }}
            {{ Form::password('password', array('class' => '', 'placeholder' => 'Password')) }}
            <br />
            {{ Form::submit('Sign in', array('class' => 'btn')) }}
        </fieldset>
    {{ Form::close() }}
@endsection
