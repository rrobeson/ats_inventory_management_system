@layout('master')

@section('content')
    <h1 class="page-header">
        Manage Items
        <p>
            <a href="{{ URL::to_action('items/new') }}" class="btn btn-primary">
                New Item
            </a>
        </p>
    </h1>

    @if (Session::get('success_message'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <p>
            {{ Session::get('success_message') }}
        </p>
    </div>
    @endif

    @if (Session::get('error_message'))
    <div class="alert alert-error alert-block">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <p>
            {{ Session::get('error_message') }}
        </p>
    </div>
    @endif

    <table id="items" class="datatable table table-striped table-bordered table-hover" data-datatable-not-sortable="8" data-datatable-not-visible="9,10,11">
        <thead>
            <tr>
                <th>ETSU Number</th>
                <th>Name</th>
                <th>Serial Number</th>
                <th style="text-align:right">Value</th>
                <th>Category</th>
                <th style="text-align:right">Allowed Checkout Length(days)</th>
                <th>Status</th>
                <th>Available?</th>
                <th>Actions</th>
                <th>Keywords</th>
                <th>Description</th>
                <th>Comments</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($items as $i)
            <tr class=" ">
                <td>{{ strtoupper(e($i->inventory_number)) }}</td>
                <td>{{ e($i->name) }}</td>
                <td>{{ e($i->serial_number) }}</td>
                <td style="text-align:right">{{ money_format('$%i', e($i->monetary_value)) }}</td>
                <td>{{ e($i->category()->first()->name) }}</td>
                <td style="text-align:right">{{ e($i->allowed_checkout_length) }}</td>
                <td>{{ ucfirst(e($i->status)) }}</td>
                <td>
                @if ($i->availability)
                    Yes
                @else
                    No
                @endif
                </td>
                <td>
                    {{ Form::open('items/delete', 'POST', array('class' => '')) }}
                    <div class="btn-group">
                        <a class="btn btn-small" href="{{ URL::to_action('items/show', array($i->id)) }}">
                            Show
                        </a>
                        <a class="btn btn-small" href="{{ URL::to_action('items/edit', array($i->id)) }}">
                            Edit
                        </a>
                        {{ Form::hidden('id', $i->id) }}
                        {{ Form::submit('Delete', array('class' => 'btn btn-danger btn-small')) }}
                    </div>
                    {{ Form::close() }}
                </td>
                <td>{{ e($i->keywords) }}</td>
                <td>{{ e($i->description) }}</td>
                <td>{{ e($i->comments) }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection

@section('page_specific_js')
@endsection
