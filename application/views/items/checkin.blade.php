@layout('master')

@section('content')
    <div class="page-header">
        <h1>Checkin Item</h1>
    </div>

    @if (Session::get('success_message'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <p>
            {{ Session::get('success_message') }}
        </p>
    </div>
    @endif

    @if (Session::get('error_message'))
    <div class="alert alert-error alert-block">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <p>
            {{ Session::get('error_message') }}
        </p>
    </div>
    @endif

    {{ Form::open('items/confirm_checkin', 'POST', array('class' => '')) }}
        <fieldset>
            @if (Session::get('errors'))
            <div class="alert alert-error alert-block">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
            </div>
            @endif    

            <div class="control-group {{ $errors->has('inventory_number') ? 'error' : '' }}">
                {{ Form::label('inventory_number', 'Inventory Number', array('class'=> 'control-label')) }}
                <div class="controls">
                    {{ Form::text('inventory_number', Input::old("inventory_number"), array('required' => 'required', 'pattern' => '.{6}', 'title' => 'The inventory number must be six digits', 'placeholder' => '123456', 'data-provide' => 'typeahead', 'data-source' => json_encode($inventory_numbers), 'autocomplete' => 'off')) }}
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    {{ Form::submit('Continue', array('class' => 'btn btn-primary')) }}
                    <a href="{{ URL::to_action('admin/index') }}" class="btn">Cancel</a>
                </div>
            </div>
        </fieldset>
    {{ Form::close() }}
@endsection

@section('page_specific_js')
@endsection
