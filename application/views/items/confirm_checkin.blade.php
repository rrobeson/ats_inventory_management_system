@layout('master')

@section('content')
    <h1 class="page-header">
        Checkin Item: {{ e($item->name) }}
    </h1>

    @if (Session::get('success_message'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <p>
            {{ Session::get('success_message') }}
        </p>
    </div>
    @endif

    @if (Session::get('error_message'))
    <div class="alert alert-error alert-block">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <p>
            {{ Session::get('error_message') }}
        </p>
    </div>
    @endif

    <p>
        <strong>Inventory Number:</strong>
        {{ strtoupper(e($item->inventory_number)) }}
    </p>

    <p>
        <strong>Serial Number:</strong>
        {{ e($item->serial_number) }}
    </p>

    <p>
        <strong>Category:</strong>
        {{ e($item->category()->first()->name) }}
    </p>

    <p>
        <strong>Description:</strong>
        {{ e($item->description) }}
    </p>

    <p>
        <strong>Comments:</strong>
        {{ e($item->comments) }}
    </p>

    <p>
        <strong>Availability:</strong>
        @if ($item->availability)
            Yes
        @else
            No
        @endif
    </p>
    
    {{ Form::open('items/checkin', 'POST') }}
        <fieldset>
            @if (Session::get('errors'))
            <div class="alert alert-error alert-block">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
            </div>
            @endif    
            <input type="hidden" name="item_id" value="{{ $item->id }}" />
            <div class="control-group">
                <div class="controls">
                    {{ Form::submit('Confirm', array('class' => 'btn btn-primary')) }}
                    <a href="{{ URL::to_action('items/checkin') }}" class="btn">Cancel</a>
                </div>
            </div>
        </fieldset>
    {{ Form::close() }}

@endsection

@section('page_specific_js')
@endsection
