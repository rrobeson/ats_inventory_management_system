@layout('master')

@section('content')
    <div class="page-header">
        <h1>New Item</h1>
    </div>
    {{ Form::open('items/new', 'POST', array('class' => '')) }}
        <fieldset>
            @if (Session::get('errors'))
            <div class="alert alert-error alert-block">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
            </div>
            @endif    

            <div class="control-group {{ $errors->has('inventory_number') ? 'error' : '' }}">
                {{ Form::label('inventory_number', 'Inventory Number', array('class'=> 'control-label')) }}
                <div class="controls">
                    {{ Form::text('inventory_number', Input::old("inventory_number"), array('required' => 'required', 'pattern' => '.{6}', 'title' => 'The inventory number must be six digits', 'placeholder' => '123456')) }}
                </div>
            </div>
            <div class="control-group {{ $errors->has('name') ? 'error' : '' }}">
                {{ Form::label('name', 'Name', array('class'=> 'control-label')) }}
                <div class="controls">
                    {{ Form::text('name', Input::old("name"), array('required' => 'required')) }}
                </div>
            </div>
            <div class="control-group {{ $errors->has('serial_number') ? 'error' : '' }}">
                {{ Form::label('serial_number', 'Serial Number', array('class'=> 'control-label')) }}
                <div class="controls">
                    {{ Form::text('serial_number', Input::old("serial_number"), array()) }}
                </div>
            </div>
            <div class="control-group {{ $errors->has('allowed_checkout_length') ? 'error' : '' }}">
                {{ Form::label('allowed_checkout_length', 'Allowed Checkout Length(days)', array('class' => 'control-label')) }}
                <div class="controls">
                    {{ Form::text('allowed_checkout_length', Input::old("allowed_checkout_length"), array('placeholder' => '7', 'required' => 'required')) }}
                </div>
            </div>
            <div class="control-group {{ $errors->has('status') ? 'error' : '' }}">
                {{ Form::label('status', 'Status', array('class' => 'control-label')) }}
                <div class="controls">
                    <select name="status" required="required">
                    @foreach ($statuses as $s)
                    <option value="{{ $s }}"
                    @if (strtoupper(Input::old('status')) == strtoupper($s))
                        selected="selected"
                    @endif
                    >
                        {{ ucfirst($s) }}
                    </option>
                    @endforeach
                    </select>
                </div>
            </div>
            <div class="control-group {{ $errors->has('category') ? 'error' : '' }}">
                {{ Form::label('category', 'Category', array('class' => 'control-label')) }}
                <div class="controls">
                    <select name="category" required="required">
                        <option></option>
                        @foreach ($categories as $c)
                        <option value="{{ e($c->id) }}"
                        @if (Input::old('category') == $c->id)
                            selected="selected"
                        @endif
                        >{{ e($c->name) }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="control-group {{ $errors->has('description') ? 'error' : '' }}">
                {{ Form::label('description', 'Description', array('class' => 'control-label')) }}
                <div class="controls">
                    {{ Form::textarea('description', Input::old("description"), array('required' => 'required')) }}
                </div>
            </div>
            <div class="control-group {{ $errors->has('monetary_value') ? 'error' : '' }}">
                {{ Form::label('monetary_value', 'Monetary Value', array('class' => 'control-label')) }}
                <div class="controls">
                    {{ Form::text('monetary_value', Input::old("monetary_value"), array()) }}
                </div>
            </div>
            <div class="control-group {{ $errors->has('keywords') ? 'error' : '' }}">
                {{ Form::label('keywords', 'Keywords', array('class' => 'control-label')) }}
                <div class="controls">
                    {{ Form::textarea('keywords', Input::old("keywords"), array()) }}
                </div>
            </div>
            <div class="control-group {{ $errors->has('comments') ? 'error' : '' }}">
                {{ Form::label('comments', 'Comments', array('class' => 'control-label')) }}
                <div class="controls">
                    {{ Form::textarea('comments', Input::old("comments"), array()) }}
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    {{ Form::submit('Create', array('class' => 'btn btn-primary')) }}
                    <a href="{{ URL::to_action('items/index') }}" class="btn">Cancel</a>
                </div>
            </div>
        </fieldset>
    {{ Form::close() }}
@endsection

@section('page_specific_js')
@endsection
