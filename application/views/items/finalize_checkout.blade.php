@layout('master')

@section('content')
    <div class="page-header">
        <h1>
            Checkout Item: {{ Input::old('item')->name }}
        </h1>
        <h2>
            Due Date: {{ e(date('l F j, Y', strtotime('+ ' . Input::old('item')->allowed_checkout_length . ' days'))) }}
        </h2>
        <ul class="unstyled">
            <li>
                Inventory Number: {{ Input::old('item')->inventory_number }}
            </li>
            <li>
                Serial Number: {{ Input::old('item')->serial_number }}
            </li>
            <li>
                Description: <p>{{ Input::old('item')->description }}</p>
            </li>
        </ul>
    </div>
    {{ Form::open('items/finalize_checkout', 'POST', array('class' => '')) }}
        <fieldset>
            @if (Session::get('errors'))
            <div class="alert alert-error alert-block">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
            </div>
            @endif    

            <div class="control-group {{ $errors->has('faculty_id') ? 'error' : '' }}">
                {{ Form::label('', 'Faculty ID', array('class'=> 'control-label')) }}
                <div class="controls">
                    {{ Form::text('', Input::old('faculty_id') , array('disabled'=>'disabled')) }}
                </div>
            </div>

            @if (Input::old('nf'))
            <div class="control-group {{ $errors->has('first_name') ? 'error' : ''}}">
                {{ Form::label('first_name', 'First Name', array('class="control-label')) }}
                <div class="controls">
                    {{ Form::text('first_name', Input::old('first_name'), array('required' => 'required')) }}
                </div>
            </div>
            <div class="control-group {{ $errors->has('last_name') ? 'error' : ''}}">
                {{ Form::label('last_name', 'Last Name', array('class="control-label')) }}
                <div class="controls">
                    {{ Form::text('last_name', Input::old('last_name'), array('required' => 'required')) }}
                </div>
            </div>
            <div class="control-group {{ $errors->has('email_address') ? 'error' : ''}}">
                {{ Form::label('email_address', 'Email Address', array('class="control-label')) }}
                <div class="controls">
                    {{ Form::text('email_address', Input::old('email_address'), array('required' => 'required')) }}
                </div>
            </div>
            <div class="control-group {{ $errors->has('department') ? 'error' : ''}}">
                {{ Form::label('department', 'Department', array('class="control-label')) }}
                <div class="controls">
                    {{ Form::text('department', Input::old('department'), array('required' => 'required')) }}
                </div>
            </div>
            <div class="control-group {{ $errors->has('phone_number') ? 'error' : ''}}">
                {{ Form::label('phone_number', 'Phone Number', array('class="control-label')) }}
                <div class="controls">
                    {{ Form::text('phone_number', Input::old('phone_number'), array('required' => 'required', 'pattern' => '.{10}', 'title' => 'The phone number should be of the form 4235551234')) }}
                </div>
            </div>
            <div class="control-group {{ $errors->has('barcode_number') ? 'error' : ''}}">
                {{ Form::label('barcode_number', 'Barcode Number', array('class="control-label')) }}
                <div class="controls">
                    {{ Form::text('barcode_number', Input::old('barcode_number'), array('required' => 'required')) }}
                </div>
            </div>
            @else
            <div class="control-group {{ $errors->has('first_name') ? 'error' : ''}}">
                {{ Form::label('first_name', 'First Name', array('class="control-label')) }}
                <div class="controls">
                    {{ Form::text('first_name', Input::has('first_name') ? Input::old('first_name') : Input::old('faculty')->first_name, array('required' => 'required')) }}
                </div>
            </div>
            <div class="control-group {{ $errors->has('last_name') ? 'error' : ''}}">
                {{ Form::label('last_name', 'Last Name', array('class="control-label')) }}
                <div class="controls">
                    {{ Form::text('last_name', Input::has('last_name') ? Input::old('last_name') : Input::old('faculty')->last_name, array('required' => 'required')) }}
                </div>
            </div>
            <div class="control-group {{ $errors->has('email_address') ? 'error' : ''}}">
                {{ Form::label('email_address', 'Email Address', array('class="control-label')) }}
                <div class="controls">
                    {{ Form::text('email_address', Input::has('email_address') ? Input::old('email_address') : Input::old('faculty')->email_address, array('required' => 'required')) }}
                </div>
            </div>
            <div class="control-group {{ $errors->has('department') ? 'error' : ''}}">
                {{ Form::label('department', 'Department', array('class="control-label')) }}
                <div class="controls">
                    {{ Form::text('department', Input::has('department') ? Input::old('department') : Input::old('faculty')->department, array('required' => 'required')) }}
                </div>
            </div>
            <div class="control-group {{ $errors->has('phone_number') ? 'error' : ''}}">
                {{ Form::label('phone_number', 'Phone Number', array('class="control-label')) }}
                <div class="controls">
                    {{ Form::text('phone_number', Input::has('phone_number') ? Input::old('phone_number') : Input::old('faculty')->phone_number, array('required' => 'required', 'pattern' => '.{10}', 'title' => 'The phone number should be of the form 4235551234')) }}
                </div>
            </div>
            <div class="control-group {{ $errors->has('barcode_number') ? 'error' : ''}}">
                {{ Form::label('barcode_number', 'Barcode Number', array('class="control-label')) }}
                <div class="controls">
                    {{ Form::text('barcode_number', Input::has('barcode_number') ? Input::old('barcode_number') : Input::old('faculty')->barcode_number, array('required' => 'required')) }}
                </div>
            </div>
            @endif
            {{ Form::hidden('new_faculty', Input::old('nf')) }}
            {{ Form::hidden('faculty_id', Input::old('faculty_id')) }}
            {{ Form::hidden('item_id', Input::old('item')->id) }}
            {{ Form::hidden('inventory_number', Input::old('item')->inventory_number) }}
            <div class="control-group">
                <div class="controls">
                    {{ Form::submit('Checkout', array('class' => 'btn btn-primary')) }}
                    <a href="{{ URL::to_action('items/checkout') }}" class="btn">Cancel</a>
                </div>
            </div>
        </fieldset>
    {{ Form::close() }}
@endsection

@section('page_specific_js')
@endsection
