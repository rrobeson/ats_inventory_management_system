@layout('master')

@section('content')
    <h1 class="page-header">
        Item: {{ e($item->name) }}
    </h1>

    <p>
        <strong>Inventory Number:</strong>
        {{ strtoupper(e($item->inventory_number)) }}
    </p>

    <p>
        <strong>Serial Number:</strong>
        {{ e($item->serial_number) }}
    </p>

    <p>
        <strong>Monetary Value:</strong>
        {{ money_format('$%i', e($item->monetary_value)) }}
    </p>

    <p>
        <strong>Category:</strong>
        {{ e($item->category()->first()->name) }}
    </p>

    <p>
        <strong>Keywords:</strong>
        {{ e($item->keywords) }}
    </p>

    <p>
        <strong>Allowed Checkout Length:</strong>
        {{ e($item->allowed_checkout_length) }}
        days
    </p>

    <p>
        <strong>Description:</strong>
        {{ e($item->description) }}
    </p>

    <p>
        <strong>Comments:</strong>
        {{ e($item->comments) }}
    </p>

    <p>
        <strong>Status:</strong>
        {{ ucfirst(e($item->status)) }}
    </p>

    <p>
        <strong>Availability:</strong>
        @if ($item->availability)
            Yes
        @else
            No
        @endif
    </p>

    @if($checkout_information)
        <h2>Checkout Details</h2>
        @foreach($checkout_information as $c)
        <dl>
            <dt>
                <strong>Checked Out On:</strong>
            </dt>
            <dd>
                {{ e(date('l F j, Y', strtotime($c->checked_out_date))) }}
            </dd>
            <dt>
                <strong>Due:</strong>
            </dt>
            <dd>
                {{ e(date('l F j, Y', strtotime($c->renewal_due_date ? $c->renewal_due_date : $c->due_date))) }}
            </dd>
            <dt>
                <strong>Checked Out To:</strong>
            </dt>
            <dd>
                {{ e($c->checked_out_to()->first()->first_name) }}
                {{ e($c->checked_out_to()->first()->last_name) }}
                <br />
                Phone: {{ e($c->checked_out_to()->first()->phone_number) }}
                <br />
                Email: {{ e($c->checked_out_to()->first()->email_address) }}
                <br />
                ID: {{ e($c->checked_out_to()->first()->faculty_id) }}
            </dd>
            <dt>
                <strong>Checked Out By:</strong>
            </dt>
            <dd>
                {{ e($c->checked_out_by()->first()->first_name) }}
                {{ e($c->checked_out_by()->first()->last_name) }}
                <br />
                Email: {{ e($c->checked_out_by()->first()->email) }}
            </dd>
        </dl>
        @endforeach
    @endif

    @if($holds)
        <h2>Holds</h2>
        @foreach($holds as $h)
        <div class="row">
            <dl class="well span4">
                <dt>
                    <strong>Dates:</strong>
                </dt>
                <dd>
                    {{ e(date('l F j, Y', strtotime($h->start_date))) }}
                    -
                    {{ e(date('l F j, Y', strtotime($h->end_date))) }}
                </dd>
                <dt>
                    <strong>Held For:</strong>
                </dt>
                <dd>
                    {{ e($h->held_for()->first()->first_name) }}
                    {{ e($h->held_for()->first()->last_name) }}
                    -
                    {{ e($h->held_for()->first()->email_address) }}
                </dd>
                <dt>
                    <strong>Approved By:</strong>
                </dt>
                <dd>
                    {{ e($h->approved_or_denied_by()->first()->first_name) }}
                    {{ e($h->approved_or_denied_by()->first()->last_name) }}
                    -
                    {{ e($h->approved_or_denied_by()->first()->email) }}
                </dd>
            </dl>
        </div>
        @endforeach
    @endif

    <div class="btn-group">
    @if(Helperss::is_admin())
        <a href="{{ URL::to_action('items/edit', array('id' => $item->id)) }}" class="btn">Edit</a>
    @endif
        <a href="{{ URL::to_action('items/index') }}" class="btn">Back</a>
    </div>
@endsection

@section('page_specific_js')
@endsection
