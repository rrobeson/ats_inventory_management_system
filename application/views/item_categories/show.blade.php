@layout('master')

@section('content')
    <h1 class="page-header">
        Category: {{ e($category->name) }}
    </h1>

    <p>
        <strong>Description:</strong>
        {{ e($category->description) }}
    </p>

    <div class="btn-group">
        <a href="{{ URL::to_action('item_categories/edit', array('id' => $category->id)) }}" class="btn">Edit</a>
        <a href="{{ URL::to_action('item_categories/index') }}" class="btn">Back</a>
    </div>
@endsection

@section('page_specific_js')
@endsection
