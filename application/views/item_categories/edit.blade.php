@layout('master')

@section('content')
    <div class="page-header">
        <h1>Edit {{ e($category->name) }}</h1>
    </div>
    {{ Form::open('item_categories/edit', 'POST', array('class' => '')) }}
        <fieldset>
            @if (Session::get('errors'))
            <div class="alert alert-error alert-block">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
            </div>
            @endif    

            <div class="control-group {{ $errors->has('name') ? 'error' : '' }}">
                {{ Form::label('name', 'Name', array('class'=> 'control-label')) }}
                <div class="controls">
                    {{ Form::text('name', Input::has("name") ? Input::old("name") : $category->name, array('required' => 'required')) }}
                </div>
            </div>
            <div class="control-group {{ $errors->has('description') ? 'error' : '' }}">
                {{ Form::label('description', 'Description', array('class' => 'control-label')) }}
                <div class="controls">
                    {{ Form::textarea('description', Input::has("description") ? Input::old("description") : $category->description, array('required' => 'required')) }}
                </div>
            </div>
            {{ Form::hidden('id', $category->id) }}
            <div class="control-group">
                <div class="controls">
                    {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
                    <a href="{{ URL::to_action('item_categories/index') }}" class="btn">Cancel</a>
                </div>
            </div>
        </fieldset>
    {{ Form::close() }}
@endsection

@section('page_specific_js')
@endsection
