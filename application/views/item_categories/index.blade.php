@layout('master')

@section('content')
    <h1 class="page-header">
        Manage Item Categories
        <p>
            <a href="{{ URL::to_action('item_categories/new') }}" class="btn btn-primary">
                New Item Category
            </a>
        </p>
    </h1>

    @if (Session::get('success_message'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <p>
            {{ Session::get('success_message') }}
        </p>
    </div>
    @endif

    @if (Session::get('error_message'))
    <div class="alert alert-error alert-block">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <p>
            {{ Session::get('error_message') }}
        </p>
    </div>
    @endif

    <table id="categories" class="datatable table table-striped table-bordered table-hover" data-datatable-not-sortable="1">
        <thead>
            <tr>
                <th>Name</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($categories as $c)
            <tr class=" ">
                <td>{{ e($c->name) }}</td>
                <td>
                    {{ Form::open('item_categories/delete', 'POST', array('class' => '')) }}
                    <div class="btn-group">
                        <a class="btn btn-small" href="{{ URL::to_action('item_categories/show', array($c->id)) }}">
                            Show
                        </a>
                        <a class="btn btn-small" href="{{ URL::to_action('item_categories/edit', array($c->id)) }}">
                            Edit
                        </a>
                            {{ Form::hidden('id', $c->id) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger btn-small')) }}
                    </div>
                    {{ Form::close() }}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection

@section('page_specific_js')
    <script>
    </script>
@endsection
