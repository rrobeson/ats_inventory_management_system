@layout('master')

@section('content')
    <div class="page-header">
        <h1>New Item Category</h1>
    </div>
    {{ Form::open('item_categories/new', 'POST', array('class' => '')) }}
        <fieldset>
            @if (Session::get('errors'))
            <div class="alert alert-error alert-block">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
            </div>
            @endif    

            <div class="control-group {{ $errors->has('name') ? 'error' : '' }}">
                {{ Form::label('name', 'Name', array('class'=> 'control-label')) }}
                <div class="controls">
                    {{ Form::text('name', Input::old("name"), array('required' => 'required')) }}
                </div>
            </div>
            <div class="control-group {{ $errors->has('description') ? 'error' : '' }}">
                {{ Form::label('description', 'Description', array('class' => 'control-label')) }}
                <div class="controls">
                    {{ Form::textarea('description', Input::old("description"), array('required' => 'required')) }}
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    {{ Form::submit('Create', array('class' => 'btn btn-primary')) }}
                    <a href="{{ URL::to_action('item_categories/index') }}" class="btn">Cancel</a>
                </div>
            </div>
        </fieldset>
    {{ Form::close() }}
@endsection

@section('page_specific_js')
@endsection
