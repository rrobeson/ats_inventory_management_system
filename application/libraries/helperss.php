<?php

class Helperss {
    public static function is($title) {
        # See if a user is logged in
        if (!Sentry::check()) {
            return false;
        }

        $user_permissions = json_decode(Sentry::user()->permissions(), true);

        # See if the title exists in the permissions
        if (!array_key_exists($title, $user_permissions)) {
            return false;
        }

        # See if the user has the title
        return $user_permissions[$title];
    }

    public static function is_super_admin() {
        return Helperss::is('is_super_admin');
    }

    public static function is_admin() {
        return Helperss::is('is_admin');
    }

    public static function is_read_only_admin() {
        return Helperss::is('is_read_only_admin');
    }
}
