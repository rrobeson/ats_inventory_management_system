<?php

class Users_Controller extends Base_Controller {
    public $restful = true;

    public function __construct() 
    {
        $this->filter('before', 'logged_in');
        $this->filter('before', 'super_admin_only');
    }

	public function get_index()
	{
        $users = User::order_by('last_name', 'asc')->get();

		return View::make('users.index')->with('users', $users);
	}

    public function get_show($id)
    {
        $user = User::find($id);

        return View::make('users.show')->with('user', $user);
    }

    public function get_new()
    {
        return View::make('users.new');
    }

    public function post_new()
    {
        // Get the values that are valid access levels
        $valid_access_levels = implode(",", array_values(User::valid_access_levels()));

        $rules = array(
            'email' => 'required|email|unique:users',
            'first_name' => 'required',
            'last_name' => 'required',
            'userid' => 'required|size:9|match:/^E.{8}$/|unique:users',
            'access_level' => 'required|in:' . $valid_access_levels,
            'password' => 'required|confirmed'
        );

        $input = Input::get();
        $validation = Validator::make($input, $rules);

        if($validation->fails()) {
            return Redirect::to_action('users/new')->with_input()->with_errors($validation);
        }

        try {
            // Create the user with Sentry to setup everything
            // correctly.
            Sentry::user()->create(array(
                'email' => $input['email'],
                'password' => $input['password']
            ));

            // Find the just created user and update the rest
            // of the fields.
            $u = User::where_email($input['email'])->first();
            $u->first_name = $input['first_name'];
            $u->last_name = $input['last_name'];
            $u->userid = $input['userid'];
            $u->set_access_level($input['access_level']);
            $u->save();

            return Redirect::to_action('users/index')->with('success_message', 'Item created successfully');
        } catch (Exception $e) {
            $errors = new Laravel\Messages();
            $errors->add('save_error', $e->getMessage());
            return Redirect::to_action('users/new')->with_input()->with_errors($errors);
        }
    }

    public function get_edit($id)
    {
        $user = User::find($id);

        return View::make('users.edit')->with('user', $user);
    }

    public function post_edit()
    {
        // Get the values that are valid access levels
        $valid_access_levels = implode(",", array_values(User::valid_access_levels()));

        //do validation
        $rules = array(
            'id' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'access_level' => 'required|in:' . $valid_access_levels
        );

        $input = Input::get();
        $validation = Validator::make($input, $rules);

        if($validation->fails()) {
            return Redirect::to_action('users/edit', array($input['id']))->with_input()->with_errors($validation);
        }

        try {
            $u = User::find($input['id']);
            $u->first_name = $input['first_name'];
            $u->last_name = $input['last_name'];
            $u->set_access_level($input['access_level']);
            $u->save();

            return Redirect::to_action('users/index');
        } catch (Exception $e) {
            $errors = new Laravel\Messages();
            $errors->add('save_error', $e->getMessage());
            return Redirect::to_action('users/edit', array($input['id']))->with_errors($errors);
        }
    }

    public function post_deactivate()
    {
        $rules = array(
            'id' => 'required|not_in:' . Sentry::user()->id
        );

        $input = Input::get();
        $validation = Validator::make($input, $rules);

        if($validation->fails()) {
            return Redirect::to_action('users/index', array($input['id']))->with_input()->with_errors($validation);
        }

        try {
            $u = User::find($input['id']);
            $u->activated = 0;
            $u->save();

            return Redirect::to_action('users/index')->with('success_message', 'User deactivated successfully');
        } catch (Exception $e) {
            $errors = new Laravel\Messages();
            $errors->add('save_error', $e->getMessage());
            return Redirect::to_action('users/index', array($input['id']))->with_errors($errors);
        }
    }

    public function post_activate()
    {
        $rules = array(
            'id' => 'required|not_in:' . Sentry::user()->id
        );

        $input = Input::get();
        $validation = Validator::make($input, $rules);

        if($validation->fails()) {
            return Redirect::to_action('users/index', array($input['id']))->with_input()->with_errors($validation);
        }

        try {
            $u = User::find($input['id']);
            $u->activated = 1;
            $u->save();

            return Redirect::to_action('users/index')->with('success_message', 'User activated successfully.');
        } catch (Exception $e) {
            $errors = new Laravel\Messages();
            $errors->add('save_error', $e->getMessage());
            return Redirect::to_action('users/index', array($input['id']))->with_errors($errors);
        }
    }

    public function get_change_password($id)
    {
        $user = User::find($id);

        return View::make('users.change_password')->with('user', $user);
    }

    public function post_change_password()
    {
        //do validation
        $rules = array(
            'id' => 'required|exists:users',
            'password' => 'required|confirmed',
        );

        $input = Input::get();
        $validation = Validator::make($input, $rules);

        if($validation->fails()) {
            return Redirect::to_action('users/change_password', array($input['id']))->with_input()->with_errors($validation);
        }

        try {
            $u = User::find($input['id']);
            Sentry::user($u->email)->update(array('password' => $input['password']));

            return Redirect::to_action('users/index')->with('success_message', 'Password changed successfully');
        } catch (Exception $e) {
            $errors = new Laravel\Messages();
            $errors->add('save_error', $e->getMessage());
            return Redirect::to_action('users/change_password', array($input['id']))->with_errors($errors);
        }
    }
}
