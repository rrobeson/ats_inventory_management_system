<?php

class Holds_Controller extends Base_Controller {
    public $restful = true;

	public function get_index()
	{
        $items = Item::good_condition()->order_by('name', 'asc')->get();

        return View::make('holds.index')->with('items', $items);
	}

    public function get_pending()
    {
        $holds = Hold::pending()
            ->get();

        return View::make('holds.pending')->with('holds', $holds);
    }

    public function post_complete()
    {
        $rules = array(
            'id' => 'required|exists:holds'
        );

        $input = Input::get();
        $validation = Validator::make($input, $rules);

        if($validation->fails()) {
            return Redirect::to_action('holds/pending')
                ->with('error_message', 'Cannot complete a hold that does not exist. (Invalid hold ID)');
        }

        try {
            $h = Hold::find($input['id']);
            $h->status = 'completed';
            $h->save();

            return Redirect::to_action('holds/pending')
                ->with('success_message', 'Hold completed.');
        } catch (Exception $e) {
            return Redirect::to_action('holds/pending')
                ->with('error_message', 'Something went wrong. Could not complete hold.');
        }

    }

    public function get_request_hold($id)
    {
        $item = Item::find($id);
        return View::make('holds.request_hold')->with('item', $item);
    }

    public function post_request_hold()
    {
        $rules = array(
            'item_id' => 'required|exists:items,id', 
            'faculty_id' => 'required|size:9|match:/^E.{8}$/',
            'first_name' => 'required',
            'last_name' => 'required',
            'email_address' => 'required|email',
            'department' => 'required',
            'phone_number' => 'required|size:10',
            'start_date' => 'required|match:/^\d\d\/\d\d\/\d{4}$/',
            'end_date' => 'required|match:/^\d\d\/\d\d\/\d{4}$/'
        );

        $input = Input::get();
        $validation = Validator::make($input, $rules);

        if(!array_key_exists('item_id', $input)) {
            return Redirect::to_action('holds/index');
        }

        if($validation->fails()) {
            return Redirect::to_action('holds/request_hold', array($input['item_id']))->with_input()->with_errors($validation);
        }

        try {
            $f = Faculty::where_faculty_id($input['faculty_id']);
            $new = false;
            if ($f->count() < 1) {
                $f = new Faculty;
                $new = true;
            } else {
                $f = $f->first();
            }

            if ($new) {
                $f->faculty_id = $input['faculty_id'];
            }
            $f->first_name = $input['first_name'];
            $f->last_name = $input['last_name'];
            $f->email_address = $input['email_address'];
            $f->department = $input['department'];
            $f->phone_number = $input['phone_number'];
            $f->save();

            $item = Item::find($input['item_id']);
            $hold = new Hold;
            $hold->start_date = date('Y-m-d H:i:s', strtotime($input['start_date']));
            $hold->end_date = date('Y-m-d H:i:s', strtotime($input['end_date']));
            $hold->held_for = $f->id;
            $hold->item = $input['item_id'];
            $hold->status = 'pending';
            $hold->save();
            
            return View::make('holds/request_hold_confirmation');
        } catch (Exception $e) {
            $errors = new Laravel\Messages();
            $errors->add('save_error', $e->getMessage());
            return Redirect::to_action('holds/request_hold', array($input['item_id']))->with_input()->with_errors($errors);
        }
    }

    public function post_approve()
    {
        $rules = array(
            'id' => 'required|exists:holds,id'
        );

        $input = Input::get();
        $validation = Validator::make($input, $rules);

        if($validation->fails()) {
            return Redirect::to_action('holds/pending')->with('error_message', 'Could not approve hold. Something went wrong.');
        }

        try {
            $h = Hold::find($input['id']);
            $hold_type = $h->type();

            if ($hold_type == 'hold') {
                $h->approved_or_denied_by = Sentry::user()->get('id');
                $h->status = 'approved';
                $h->date_approved = date('Y-m-d H:i:s');
                $h->save();

                return Redirect::to_action('holds/pending')->with('success_message', 'Hold approved.');
            } elseif ($hold_type == 'renewal') {
                $h->approved_or_denied_by = Sentry::user()->get('id');
                $h->status = 'completed';
                $h->date_approved = date('Y-m-d H:i:s');
                $h->save();

                $i = $h->item()->first();
                $c = $i->current_check_out();
                $c->checked_out_date = $c->checked_out_date;
                $c->renewal_due_date = date('Y-m-d H:i:s', strtotime($h->end_date));
                $c->renewal_checked_out_by = Sentry::user()->get('id');
                $c->save();

                return Redirect::to_action('holds/pending')->with('success_message', 'Renewal approved.');
            }
        } catch (Exception $e) {
            return Redirect::to_action('holds/pending')->with('error_message', $e->getMessage());
        }

    }

    public function post_deny()
    {
        $rules = array(
            'id' => 'required|exists:holds,id'
        );

        $input = Input::get();
        $validation = Validator::make($input, $rules);

        if($validation->fails()) {
            return Redirect::to_action('holds/pending')->with('error_message', 'Could not deny hold. Something went wrong.');
        }

        try {
            $h = Hold::find($input['id']);
            $h->approved_or_denied_by = Sentry::user()->get('id');
            $h->status = 'denied';
            $h->date_approved = date('Y-m-d H:i:s');
            $h->save();

            return Redirect::to_action('holds/pending')->with('success_message', 'Hold denied.');
        } catch (Exception $e) {
            return Redirect::to_action('holds/pending')->with('error_message', $e->getMessage());
        }

    }

}
