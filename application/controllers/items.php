<?php

class Items_Controller extends Base_Controller {
    public $restful = true;

    public function __construct() 
    {
        $this->filter('before', 'logged_in');
        $this->filter('before', 'read_only_admin_only')->only(array('show'));
        $this->filter('before', 'admin_only')->except(array('show'));
    }

	public function get_index()
	{
        
        $items = Item::all();

		return View::make('items.index')->with('items', $items);
	}

    public function get_show($id)
    {
        $item = Item::find($id);

        $checkout_information = $item->check_outs()->where_null('checked_in_date')->order_by('checked_out_date')->get();

        $holds = $item->current_holds()->order_by('start_date')->get();

        return View::make('items.show')
            ->with('item', $item)
            ->with('checkout_information', $checkout_information)
            ->with('holds', $holds);
    }

    public function get_new()
    {
        $categories = ItemCategory::all();

        $statuses = array("good", "damaged", "lost", "stolen", "deleted", "surplus");

        return View::make('items.new')->with('categories', $categories)->with('statuses', $statuses);
    }

    public function post_new()
    {
        $rules = array(
            'inventory_number' => 'required|size:6|unique:items,inventory_number',
            'name' => 'required',
            'allowed_checkout_length' => 'required',
            'status' => 'required|in:good,damaged,lost,stolen,deleted,surplus',
            'category' => 'required|exists:item_categories,id',
            'description' => 'required'
        );

        $input = Input::get();
        $input['inventory_number'] = strtoupper($input['inventory_number']);
        $validation = Validator::make($input, $rules);

        if($validation->fails()) {
            return Redirect::to_action('items/new')->with_input()->with_errors($validation);
        }

        try {
            $i = new Item;
            $i->inventory_number = $input['inventory_number'];
            $i->name = $input['name'];
            $i->serial_number = $input['serial_number'];
            $i->monetary_value = $input['monetary_value'];
            $i->allowed_checkout_length = $input['allowed_checkout_length'];
            $i->status = $input['status'];
            $i->category = $input['category'];
            $i->keywords = $input['keywords'];
            $i->description = $input['description'];
            $i->comments = $input['comments'];
            $i->availability = 1;
            $i->save();

            return Redirect::to_action('items/index')->with('success_message', 'Item created successfully');
        } catch (Exception $e) {
            $errors = new Laravel\Messages();
            $errors->add('save_error', $e->getMessage());
            return Redirect::to_action('items/new')->with_input()->with_errors($errors);
        }
    }

    public function post_delete()
    {
        try {
            $item = Item::find(Input::get('id'));
            $item->delete();

            return Redirect::to_action('items/index')->with('success_message', 'Item deleted successfully');
        } catch (Exception $e) {
            $errors = new Laravel\Messages();
            $errors->add('save_error', $e->getMessage());
            return Redirect::to_action('items/index')->with('error_message', 'Item could not be deleted. Perhaps there are items that belong to this category.');
        }
    }

    public function get_edit($id)
    {
        $item = Item::find($id);

        $categories = ItemCategory::all();

        $statuses = array("good", "damaged", "lost", "stolen", "deleted", "surplus");

        return View::make('items.edit')->with('item', $item)->with('statuses', $statuses)->with('categories', $categories);
    }

    public function post_edit()
    {
        $rules = array(
            'inventory_number' => 'required|size:6|unique:items,inventory_number,' . Input::get('id'),
            'name' => 'required',
            'allowed_checkout_length' => 'required',
            'status' => 'required|in:good,damaged,lost,stolen,deleted,surplus',
            'category' => 'required|exists:item_categories,id',
            'description' => 'required'
        );

        $input = Input::get();
        $input['inventory_number'] = strtoupper($input['inventory_number']);
        $validation = Validator::make($input, $rules);

        if($validation->fails()) {
            return Redirect::to_action('items/edit')->with_input()->with_errors($validation);
        }

        try {
            $i = Item::find($input['id']);
            $i->inventory_number = $input['inventory_number'];
            $i->name = $input['name'];
            $i->serial_number = $input['serial_number'];
            $i->allowed_checkout_length = $input['allowed_checkout_length'];
            $i->status = $input['status'];
            $i->category = $input['category'];
            $i->description = $input['description'];
            $i->monetary_value = $input['monetary_value'];
            $i->keywords = $input['keywords'];
            $i->comments = $input['comments'];
            $i->save();

            return Redirect::to_action('items/index')->with('success_message', 'Item updated successfully');
        } catch (Exception $e) {
            $errors = new Laravel\Messages();
            $errors->add('save_error', $e->getMessage());
            return Redirect::to_action('items/edit', array($input['id']))->with_input()->with_errors($errors);
        }
    }

    public function get_checkin()
    {
        $inventory_numbers = [];

        if (!is_null(Item::all_checked_out())) {
            foreach(Item::all_checked_out()->get() as $item) {
                $inventory_numbers[] = $item->inventory_number;
            }
        }

        $inventory_numbers = array_unique($inventory_numbers);

        return View::make('items.checkin')->with('inventory_numbers', $inventory_numbers);
    }

    public function post_confirm_checkin()
    {
        $rules = array(
            'inventory_number' => 'required|size:6|exists:items,inventory_number'
        );

        $input = Input::get();
        $validation = Validator::make($input, $rules);

        if($validation->fails()) {
            return Redirect::to_action('items/checkin')->with_input()->with_errors($validation);
        }

        $item = Item::where_inventory_number($input['inventory_number'])->first();

        return View::make('items.confirm_checkin')->with('item', $item);
    }

    public function post_checkin()
    {
        $rules = array(
            'item_id' => 'required|exists:items,id'
        );

        $input = Input::get();
        $validation = Validator::make($input, $rules);

        if($validation->fails()) {
            return Redirect::to_action('items/checkin')->with_input()->with_errors($validation);
        }

        try {
            $i = Item::find($input['item_id']);
            $i->check_in(Sentry::user()->get('id'));

            return Redirect::to_action('items/checkin')->with('success_message', 'Item checked in successfully');
        } catch (Exception $e) {
            $errors = new Laravel\Messages();
            $errors->add('save_error', $e->getMessage());
            return Redirect::to_action('items/checkin')->with_errors($errors);
        }
    }

    public function get_checkout()
    {
        $item_ids = [];
        $faculty_ids = [];

        foreach(Item::where_availability(true)->where('status', '=', 'good')->get() as $i) {
            $item_ids[] = $i->inventory_number;
        }

        foreach(Faculty::all() as $f) {
            $faculty_ids[] = $f->faculty_id;
        }

        return View::make('items.checkout')
            ->with('item_ids', $item_ids)
            ->with('faculty_ids', $faculty_ids);
    }

    public function post_checkout()
    {
        $rules = array(
            'inventory_number' => 'required|size:6|exists:items,inventory_number', 
            'faculty_id' => 'required|size:9|match:/^E.{8}$/'
        );

        $input = Input::get();
        $validation = Validator::make($input, $rules);

        if($validation->fails()) {
            return Redirect::to_action('items/checkout')->with_input()->with_errors($validation);
        }

        $item = Item::where_inventory_number($input['inventory_number'])->first();

        // Determine if a new faculty row must be added.
        $create_new_faculty = false;
        $faculty = Faculty::where_faculty_id($input['faculty_id'])->first();
        if (is_null($faculty)) {
            $create_new_faculty = true;
        }

        // Determine if the item is on hold.
        if(!$create_new_faculty) {
            $fid = $faculty->id;
            $start_date = date('Y-m-d');
            $end_date = date('Y-m-d', strtotime(' + ' . $item->allowed_checkout_length . ' days'));
        
            if($item->is_currently_on_hold_for_a_different_faculty_member($fid, $start_date, $end_date)) {
                $on_hold_error = 'This item is currently on hold to another faculty member. '
                    . '<a target="_blank" href="show/'
                    . $item->id
                    . '">View '
                    . $item->name
                    . '</a>';
                return Redirect::to_action('items/checkout')
                    ->with('error_message', $on_hold_error);
            }
        }

        // Store the items in the input as it appears to be the best way
        // to pass them to the next step.
        Input::merge(array('item' => $item, 'faculty' => $faculty, 'nf' => $create_new_faculty));
        return Redirect::to_action('items/finalize_checkout')->with_input();
    }

    public function get_finalize_checkout()
    {
        $rules = array(
            'inventory_number' => 'required|size:6|exists:items,inventory_number', 
            'faculty_id' => 'required|size:9|match:/^E.{8}$/'
        );

        // Show an error if someone tries to visit this page directly.
        // The step should only be reached after post_checkout().
        $input = array('inventory_number' => Input::old('inventory_number'), 'faculty_id' => Input::old('faculty_id'));
        $validation = Validator::make($input, $rules);

        if($validation->fails()) {
            return Redirect::to_action('items/checkout')->with_input()->with_errors($validation);
        }
        return View::make('items.finalize_checkout');
    }

    public function post_finalize_checkout()
    {
        $rules = array(
            'inventory_number' => 'required|size:6|exists:items,inventory_number', 
            'faculty_id' => 'required|size:9|match:/^E.{8}$/',
            'first_name' => 'required',
            'last_name' => 'required',
            'email_address' => 'required',
            'department' => 'required',
            'phone_number' => 'required|size:10',
            'barcode_number' => 'required'
        );

        $input = Input::get();
        $validation = Validator::make($input, $rules);

        // Put the Item and Faculty objects back in the input
        // in case there are errors on the form.
        // Otherwise, the form will break because it cannot find the objects in the
        // old input.
        Input::merge(array('item' => Item::find($input['item_id']),
                            'faculty' => Faculty::where_faculty_id($input['faculty_id'])->first()));

        if($validation->fails()) {
            return Redirect::to_action('items/finalize_checkout')->with_input()->with_errors($validation);
        }

        try {
            // Currently this step assumes that an ATS Admin has
            // the item to be checked out in their possession
            // and therefore the item is available.
            // Perhaps the system should still verify the item's
            // availability status with the database?

            // Create a new faculty row
            // or update an existing one.
            if($input['new_faculty']) {
                $f = new Faculty;
                $f->faculty_id = $input['faculty_id'];
                $f->first_name = $input['first_name'];
                $f->last_name = $input['last_name'];
                $f->email_address = $input['email_address'];
                $f->department = $input['department'];
                $f->phone_number = $input['phone_number'];
                $f->barcode_number = $input['barcode_number'];
                $f->save();
            } else {
                $f = Faculty::where_faculty_id($input['faculty_id'])->first();
                $f->first_name = $input['first_name'];
                $f->last_name = $input['last_name'];
                $f->email_address = $input['email_address'];
                $f->department = $input['department'];
                $f->phone_number = $input['phone_number'];
                $f->barcode_number = $input['barcode_number'];
                $f->save();
            }

            // Find the item being checked out.
            $i = Item::find($input['item_id']);

            // Find the id of the ATS admin checking out the item.
            $ats_user_id = Sentry::user()->get('id');

            // Should we really automatically check an item in that
            // is still listed as checked out? If they are checking it
            // out then the Personnel should have the item in hand.
            // Therefore, if they are checking it back out, we can assume
            // that someone forgot to check it in.
            $i->check_in(Sentry::user()->get('id'));

            // Checkout the item.
            $checked_out_transaction = new CheckedOutTransaction;
            $checked_out_transaction->checked_out_to = $f->id;
            $checked_out_transaction->due_date = date('Y-m-d H:i:s', strtotime('+ ' . $i->allowed_checkout_length . ' days', time()));
            $checked_out_transaction->checked_out_by = $ats_user_id;
            $checked_out_transaction->item_checked_out = $i->id;
            $checked_out_transaction->save();

            // The item should now be unavailable
            $i->availability = false;
            $i->save();

            // Current holds should be marked as completed.
            $i->mark_holds_completed_on_checkout($f->id, date('Y-m-d'), date('Y-m-d', strtotime('+ ' . $i->allowed_checkout_length . ' days')));

            return Redirect::to_action('items/checkout')->with('success_message', 'Checkout successful!');
        } catch (Exception $e) {
            $errors = new Laravel\Messages();
            $errors->add('save_error', $e->getMessage());
            return Redirect::to_action('items/finalize_checkout')->with_input()->with_errors($errors);
        }
    }
}
