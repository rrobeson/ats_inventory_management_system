<?php

class Item_Categories_Controller extends Base_Controller {
    public $restful = true;

    public function __construct() 
    {
        $this->filter('before', 'logged_in');
        $this->filter('before', 'super_admin_only');
    }

	public function get_index()
	{
        
        $categories = ItemCategory::order_by('name')->get();

		return View::make('item_categories.index')->with('categories', $categories);
	}

    public function get_show($id)
    {
        $category = ItemCategory::find($id);

        return View::make('item_categories.show')->with('category', $category);
    }

    public function get_new()
    {
        return View::make('item_categories.new');
    }

    public function post_new()
    {
        $rules = array(
            'name' => 'required|unique:item_categories',
            'description' => 'required'
        );

        $input = Input::get();
        $validation = Validator::make($input, $rules);

        if($validation->fails()) {
            return Redirect::to_action('item_categories/new')->with_input()->with_errors($validation);
        }

        try {
            $c = new ItemCategory;
            $c->name = $input['name'];
            $c->description = $input['description'];
            $c->save();

            return Redirect::to_action('item_categories/index')->with('success_message', 'Item created successfully');
        } catch (Exception $e) {
            $errors = new Laravel\Messages();
            $errors->add('save_error', $e->getMessage());
            return Redirect::to_action('item_categories/new')->with_input()->with_errors($errors);
        }
    }

    public function post_delete()
    {
        try {
            $category = ItemCategory::find(Input::get('id'));
            $category->delete();

            return Redirect::to_action('item_categories/index')->with('success_message', 'Item deleted successfully');
        } catch (Exception $e) {
            $errors = new Laravel\Messages();
            $errors->add('save_error', $e->getMessage());
            return Redirect::to_action('item_categories/index')->with('error_message', 'Item could not be deleted. Perhaps there are items that belong to this category.');
        }
    }

    public function get_edit($id)
    {
        $category = ItemCategory::find($id);

        return View::make('item_categories.edit')->with('category', $category);
    }

    public function post_edit()
    {
        $rules = array(
            'name' => 'required|unique:item_categories,name,' . Input::get('id'),
            'description' => 'required'
        );

        $input = Input::get();
        $validation = Validator::make($input, $rules);

        if($validation->fails()) {
            return Redirect::to_action('item_categories/edit', array($input['id']))->with_input()->with_errors($validation);
        }

        try {
            $c = ItemCategory::find($input['id']);
            $c->name = $input['name'];
            $c->description = $input['description'];
            $c->save();

            return Redirect::to_action('item_categories/index')->with('success_message', 'Item updated successfully');
        } catch (Exception $e) {
            $errors = new Laravel\Messages();
            $errors->add('save_error', $e->getMessage());
            return Redirect::to_action('item_categories/edit', array($input['id']))->with_input()->with_errors($errors);
        }
    }
}
