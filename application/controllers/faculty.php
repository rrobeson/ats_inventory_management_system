<?php

class Faculty_Controller extends Base_Controller {
    public $restful = true;

    public function __construct()
    {
        $this->filter('before', 'logged_in');
        $this->filter('before', 'admin_only');
    }

    public function get_index()
    {
        $faculty = Faculty::order_by("first_name")
            ->order_by("last_name")
            ->order_by("faculty_id")
            ->order_by("department")
            ->get();

        return View::make('faculty/index')
            ->with('faculty', $faculty);
    }

    public function get_show($id)
    {
        $faculty = Faculty::find($id);

        $holds = $faculty->current_holds()
            ->order_by('start_date')
            ->get();

        $checkout_information = $faculty->current_checkouts()
            ->order_by('checked_out_date')
            ->get();

        return View::make('faculty.show')
            ->with('holds', $holds)
            ->with('checkout_information', $checkout_information)
            ->with('faculty', $faculty);
    }
}
