<?php

class Admin_Controller extends Base_Controller {
    public $restful = true;

    public function __construct() 
    {
        $this->filter('before', 'logged_in')->only(array('index'));
    }

	public function get_index()
	{
        $user_level = User::find(Sentry::user()->get('id'))->access_level();

        // Only let Read Only Admins see available items
        if ($user_level == 'Read Only Admin') {
            return Redirect::to_action('reports/available_items');
        }

        $past_due = CheckedOutTransaction::past_due()->count();

        $due_today = CheckedOutTransaction::due_today()->count();

        $pending_holds = Hold::pending()->count();

		return View::make('admin.index')
            ->with('past_due', $past_due)
            ->with('due_today', $due_today)
            ->with('pending_holds', $pending_holds)
            ->with('user_level', $user_level);
	}

    public function post_login()
    {
        // do validation
        $rules = array(
            'email' => 'required|email',
            'password' => 'required'
        );

        $input = Input::get();
        $validation = Validator::make($input, $rules);

        if ($validation->fails()) {
            return Redirect::back()
                ->with_input()
                ->with_errors($validation);
        }

        try
        {
            $valid_login = Sentry::login(Input::get('email'), Input::get('password'), true);

            if($valid_login)
            {
                return Redirect::back();
            }
            else
            {
                $data['errors'] = "Invalid login!";
            }
        }
        catch (Sentry\SentryException $e)
        {
            $data['errors'] = $e->getMessage();
        }

        return Redirect::to('home/index')->with_input()->with('errors', $data['errors']);
    }

    public function get_logout()
    {
        Sentry::logout();
        return Redirect::to('home/index');
    }
}
