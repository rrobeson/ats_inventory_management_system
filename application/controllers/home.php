<?php

class Home_Controller extends Base_Controller {
    public $restful = true;

	public function get_index()
	{
        # Redirect to the admin section if logged in
        if (Sentry::check()) {
            return Redirect::to('admin/index');
        }

        return View::make('home.index');
	}
}
