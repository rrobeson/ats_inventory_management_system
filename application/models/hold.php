<?php
class Hold extends Eloquent {
    public static $timestamps = false;

    public function held_for()
    {
        return $this->belongs_to('Faculty', 'held_for');
    }

    public function approved_or_denied_by()
    {
        return $this->belongs_to('User', 'approved_or_denied_by');
    }

    public function item()
    {
        return $this->belongs_to('Item', 'item');
    }

    /* Returns the type of the hold.
     * Either 'hold' or 'renewal'.
     * If the hold is requested by the same person
     * who currently has the item checked out and the 
     * start date of the hold is within one day of the
     * due date then the request is considered a renewal.
     * Otherwise it is treated as a normal hold request.
     */
    public function type()
    {
        $type = 'hold';

        $request_by = $this->held_for()->first()->id;
        $item = $this->item()->first();

        if ($item->checked_out())
        {
            $current_check_out = $item->current_check_out();
            $checked_out_by = $current_check_out->checked_out_to()->first()->id;

            if ($checked_out_by == $request_by)
            {
                $request_date = $this->start_date;
                $check_out_due = $current_check_out->due();

                $day_before = strtotime($check_out_due . ' - 1 days');
                $day_after = strtotime($check_out_due . ' + 1 days');

                if ((strtotime($request_date) >= $day_before) && (strtotime($request_date) <= $day_after))
                {
                    $type = 'renewal';
                }
            }
        }

        return $type;
    }

    public static function current()
    {
        return Hold::where('status', '=', 'approved')
            ->where('end_date', '>', DB::raw('NOW()'));
    }

    public static function pending()
    {
        return Hold::where('status', '=', 'pending')
            ->order_by('start_date');
    }
}
