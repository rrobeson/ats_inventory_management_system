<?php
class User extends Eloquent {

    public function check_outs() {
        return $this->has_many('CheckedOutTransaction', 'checked_out_by');
    }

    public function check_ins() {
        return $this->has_many('CheckedOutTransaction', 'checked_in_by');
    }

    public function hold_status_decisions() {
        return $this->has_many('Hold', 'approved_or_denied_by');
    }

    public function name() {
        return $this->first_name . " " . $this->last_name;
    }

    /* Returns the valid access levels any user
     * can have.
     */
    public static function valid_access_levels() {
        return array(
                     'Super Admin' => 's',
                     'Admin' => 'a',
                     'Read Only Admin' => 'r'
                    );
    }

    /* Set the user's access level based on the key
     * $level. Nothing happens if an invalid key is
     * supplied.
     * Values are: s = Super Admin
     *             a = Admin
     *             r = Read Only Admin
     */
    public function set_access_level($level) {
        $level = strtolower($level);
        if ($level === 's') {
            $super_admin = array('is_super_admin' => 1, 'is_admin' => 1, 'is_read_only_admin' => 1);
            $this->permissions = json_encode($super_admin);
        } else if ($level === 'a') {
            $admin = array('is_super_admin' => 0, 'is_admin' => 1, 'is_read_only_admin' => 1);
            $this->permissions = json_encode($admin);
        } else if ($level === 'r') {
            $read_only_admin = array('is_super_admin' => 0, 'is_admin' => 0, 'is_read_only_admin' => 1);
            $this->permissions = json_encode($read_only_admin);
        }
    }

    /* Returns human readable form of this
     * user's access level.
     */
    public function access_level() {
        $level = '';

        if($this->is_read_only_admin()) {
            $level = 'Read Only Admin';
        }

        if($this->is_admin()) {
            $level = 'Admin';
        }

        if($this->is_super_admin()) {
            $level = 'Super Admin';
        }

        return $level;
    }
    
    public function is_super_admin() {
        return $this->is('is_super_admin');
    }

    public function is_admin() {
        return $this->is('is_admin');
    }

    public function is_read_only_admin() {
        return $this->is('is_read_only_admin');
    }

    protected function is($title) {
        $user_permissions = json_decode($this->permissions, true);

        # See if the title exists in the permissions
        if (!array_key_exists($title, $user_permissions)) {
            return false;
        }

        # See if the user has the title
        return $user_permissions[$title];
    }
}
