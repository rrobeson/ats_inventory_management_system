<?php
class Faculty extends Eloquent {
    public static $table = 'faculty';

    public static $timestamps = false;

    public function checked_out_transactions()
    {
        return $this->has_many('CheckedOutTransaction', 'checked_out_to');
    }

    public function holds()
    {
        return $this->has_many('Hold', 'held_for');
    }

    public function current_holds()
    {
        return $this->holds()
            ->where('status', '=', 'approved')
            ->where('end_date', '>', DB::raw('NOW()'));
    }

    public function current_checkouts()
    {
        return $this->checked_out_transactions()
            ->where_null('checked_in_date');
    }

    public function name()
    {
        return $this->first_name . " " . $this->last_name;
    }
}
