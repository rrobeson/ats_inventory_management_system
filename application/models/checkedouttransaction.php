<?php
class CheckedOutTransaction extends Eloquent {
    public static $table = 'checked_out_transactions';

    public function checked_out_to()
    {
        return $this->belongs_to('Faculty', 'checked_out_to');
    }

    public function checked_out_by()
    {
        return $this->belongs_to('User', 'checked_out_by');
    }

    public function checked_in_by()
    {
        return $this->belongs_to('User', 'checked_in_by');
    }

    public function renewal_checked_out_by()
    {
        return $this->belongs_to('User', 'renewal_checked_out_by');
    }

    public function item_checked_out()
    {
        return $this->belongs_to('Item', 'item_checked_out');
    }

    public function due()
    {
        return $this->renewal_due_date ? $this->renewal_due_date : $this->due_date;
    }

    public static function still_checked_out()
    {
        return CheckedOutTransaction::where_null('checked_in_date');
    }

    public static function past_due()
    {
        $past_due = CheckedOutTransaction::still_checked_out()
            ->where(function($query)
            {
                $query->where_null('checked_in_date');
                $query->where_not_null('renewal_due_date');
                $query->where('renewal_due_date', '<', DB::raw('NOW()'));
            })
            ->or_where(function($query)
            {
                $query->where_null('checked_in_date');
                $query->where_null('renewal_due_date');
                $query->where('due_date', '<', DB::raw('NOW()'));
            });

        return $past_due;
    }
    
    public static function due_today()
    {
        $due_today = CheckedOutTransaction::still_checked_out()
            ->where(function($query)
            {
                $query->where_null('checked_in_date');
                $query->where_not_null('renewal_due_date');
                $query->where(DB::raw("date_format(renewal_due_date, '%m-%d-%Y')"), '=', DB::raw("date_format(NOW(), '%m-%d-%Y')"));
            })
            ->or_where(function($query)
            {
                $query->where_null('checked_in_date');
                $query->where_null('renewal_due_date');
                $query->where(DB::raw("date_format(due_date, '%m-%d-%Y')"), '=', DB::raw("date_format(NOW(), '%m-%d-%Y')"));
            });

        return $due_today;
    }

    public static function due_this_week()
    {
        $dt = strtotime(date('Y-m-d'));

        $start_date = date('N', $dt)==1 ? date('Y-m-d', $dt) : date('Y-m-d', strtotime('last monday', $dt));
        $end_date = date('N', $dt)==7 ? date('Y-m-d', $dt) : date('Y-m-d', strtotime('next sunday', $dt));

        // This query requires some fighting with the fluent query builder to get what we want.
        // Thus, the strange argument structure of a couple of the where clauses.
        $due_this_week = CheckedOutTransaction::still_checked_out()
            ->where(function($query) use ($start_date, $end_date)
            {
                $query->where_not_null('renewal_due_date');
                $query->where(DB::raw("date_format(renewal_due_date, '%Y-%m-%d') between date('" . $start_date . "')"), "and", $end_date);
            })
            ->or_where(function($query) use ($start_date, $end_date)
            {
                $query->where_null('renewal_due_date');
                $query->where(DB::raw("date_format(due_date, '%Y-%m-%d') between date('" . $start_date. "')"), "and", $end_date);
            });

        return $due_this_week;

    }

    public static function checked_out_today()
    {
        $checked_out_today = CheckedOutTransaction::where(DB::raw("date_format(checked_out_date, '%m-%d-%Y')"), '=', DB::raw("date_format(NOW(), '%m-%d-%Y')"));

        return $checked_out_today;
    }
}
