<?php
class ItemCategory extends Eloquent {
    public static $table = 'item_categories';

    public static $timestamps = false;

    public function items()
    {
        return $this->has_many('Item', 'category');
    }
}
