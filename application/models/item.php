<?php
class Item extends Eloquent {
    public static $timestamps = true;

    public function category()
    {
        return $this->belongs_to('ItemCategory', 'category');
    }

    public function check_outs()
    {
        return $this->has_many('CheckedOutTransaction', 'item_checked_out');
    }

    public function current_check_out()
    {
        return $this->check_outs()
            ->order_by('checked_out_date','desc')
            ->first();
    }

    public function checked_out()
    {
        if (!is_null($this->current_check_out())){
            if (is_null($this->current_check_out()->checked_in_date))
            {
                return true;
            }
        }

        return false;
    }

    public function holds()
    {
        return $this->has_many('Hold', 'item');
    }

    public function current_holds()
    {
        return $this->holds()
            ->where('status', '=', 'approved')
            ->where('end_date', '>', DB::raw('NOW()'));
    }

    public function is_currently_on_hold_for_a_different_faculty_member($faculty_id, $start_date, $end_date)
    {
        // How many holds are there for this item during the days it will be checked out?
        // start_date of hold after start date of checkout and before end date of checkout
        // OR
        // end_date of hold after start date of checkout and before end date of checkout
        $q = <<<EOQ
SELECT COUNT(*) count FROM `holds`
    WHERE `item` = ?
        AND `status` = 'approved'
        AND `end_date` > NOW()
        AND `held_for` <> ?
        AND (
            (date_format(start_date, '%Y-%m-%d') > ? AND date_format(start_date, '%Y-%m-%d') < ?)
            OR
            (date_format(end_date, '%Y-%m-%d') > ? AND date_format(end_date, '%Y-%m-%d') < ?))
EOQ;
        $count = DB::query($q, array($this->id, $faculty_id, $start_date, $end_date, $start_date, $end_date));

        if ($count[0]->count > 0) {
            return true;
        }

        return false;
    }

    // Mark holds as completed when this item is checked out by the person
    // who requested the hold. Only complete the holds in this time frame.
    // Not holds that this person may have 6 or 8 weeks from now.
    public function mark_holds_completed_on_checkout($faculty_id, $start_date, $end_date)
    {
        $q = <<<EOQ
SELECT id FROM `holds`
    WHERE `item` = ?
        AND `status` = 'approved'
        AND `end_date` > NOW()
        AND `held_for` = ?
        AND (
            (date_format(start_date, '%Y-%m-%d') > ? AND date_format(start_date, '%Y-%m-%d') < ?)
            OR
            (date_format(end_date, '%Y-%m-%d') > ? AND date_format(end_date, '%Y-%m-%d') < ?))
EOQ;
        $hold_ids = DB::query($q, array($this->id, $faculty_id, $start_date, $end_date, $start_date, $end_date));

        foreach($hold_ids as $id)
        {
            $h = Hold::find($id->id);
            $h->status = 'completed';
            $h->save();
        }
    }

    public static function good_condition()
    {
        return Item::where_status('good');
    }

    public static function all_checked_out()
    {
        $transaction = CheckedOutTransaction::still_checked_out()->get();
        $item_ids = [];
        foreach ($transaction as $t)
        {
            $item_ids[] = $t->item_checked_out()->first()->id;
        }

        if (empty($item_ids))
        {
            return null;
        }

        return Item::where_in('id', $item_ids);
    }

    public static function past_due()
    {
        $past_due_transactions = CheckedOutTransaction::past_due()->get();
        
        $item_ids = [];
        foreach($past_due_transactions as $t)

        {
            $item_ids[] = $t->item_checked_out;
        }

        $item_ids = array_unique($item_ids);

        if (empty($item_ids))
        {
            return null;
        }

        return Item::where_in('id', $item_ids);
    }

    public static function checked_out_today()
    {
        $transactions = CheckedOutTransaction::checked_out_today()->get();
        
        $item_ids = [];
        foreach($transactions as $t)
        {
            $item_ids[] = $t->item_checked_out;
        }

        $item_ids = array_unique($item_ids);

        if (empty($item_ids))
        {
            return null;
        }

        return Item::where_in('id', $item_ids);
    }

    public static function due_today()
    {
        $transactions = CheckedOutTransaction::due_today()->get();
        
        $item_ids = [];
        foreach($transactions as $t)
        {
            $item_ids[] = $t->item_checked_out;
        }

        $item_ids = array_unique($item_ids);

        if (empty($item_ids))
        {
            return null;
        }

        return Item::where_in('id', $item_ids);
    }

    public static function due_this_week()
    {
        $transactions = CheckedOutTransaction::due_this_week()->get();

        $item_ids = [];

        foreach($transactions as $t)
        {
            $item_ids[] = $t->item_checked_out;
        }

        $item_ids = array_unique($item_ids);

        if (empty($item_ids))
        {
            return null;
        }

        return Item::where_in('id', $item_ids);
    }

    public function check_in($ats_user_id)
    {
        $c = $this->check_outs()->where_null('checked_in_date')->get();
        foreach ($c as $check_out)
        {
            $check_out->checked_in_date = date('Y-m-d H:i:s');
            $check_out->checked_in_by = $ats_user_id;
            $check_out->save();
        }

        $this->availability = true;
        $this->save();

        return $this;
    }
}
