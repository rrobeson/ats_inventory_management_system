#!/usr/bin/env bash
rsync -a deploy/srv/salt/ root@172.16.24.145:/srv/salt/
ssh root@172.16.24.145 salt-call state.highstate --local
